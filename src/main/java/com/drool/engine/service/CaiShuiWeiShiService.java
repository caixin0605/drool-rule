package com.drool.engine.service;

import com.drool.engine.entity.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import java.text.ParseException;

/**
 * @author 菜心
 * @date 2020/8/18
 */
public interface CaiShuiWeiShiService {
    public Result cswsZjbgForSC(String bgbh,String djxh, String qymc,String nsrsbh,String zjqjq,
                                String zjqjz, String mxbh) throws ParseException;
}
