package com.drool.engine.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.drool.engine.constant.YsgjZbConstants;
import com.drool.engine.entity.*;
import com.drool.engine.mapper.hive.HiveFunctionFetch;
import com.drool.engine.mapper.mysql.*;
import com.drool.engine.mapper.oracle.Fp0001Mapper;
import com.drool.engine.service.CaiShuiWeiShiService;
import lombok.extern.slf4j.Slf4j;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.rule.FactHandle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author 菜心
 * @date 2020/8/18
 */
@Service
@Slf4j
public class CaiShuiWeiShiServiceImpl implements CaiShuiWeiShiService {
    @Resource
    private KieSession kieSession;
    @Autowired
    private HZjbgLogMapper hZjbgLogMapper;
    @Autowired
    private HZjbgMapper hZjbgMapper;
    @Autowired
    private HZjbgMxMapper hZjbgMxMapper;
    @Autowired
    private HZjbgZbyxjgbMapper hZjbgZbyxjgbMapper;
    @Autowired
    private HZbkMapper hZbkMapper;
    @Autowired
    private XtFxmxSjgxMapper xtFxmxSjgxMapper;

    @Autowired
    private HZbkJssjxMapper hZbkJssjxMapper;
    @Autowired
    private Fp0001Mapper fp0001Mapper;
    @Autowired
    YsgjZbConstants ysgjZbConstants;

    @Autowired
    private HiveFunctionFetch hiveFunctionFetch;

    /**
     * @author fly
     * @date 2020/8/21
     * 指标取数自检计算
     * 参数：报告编号 登记序号  业务区间起  业务区间止  模型编号
     */
    @Override
    @Transactional(transactionManager = "mysqlTransactionManager")
    public Result cswsZjbgForSC(String bgbh, String djxh, String qymc, String nsrsbh, String zjqjq, String zjqjz, String mxbh) throws ParseException {
        //1:日期格式化
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
       /* //获取企业基本信息
        HRelationEnterprise relationEnterprise = hZjbgMapper.queryHRelationEnterprise(djxh);
        if(relationEnterprise==null){
            log.error("登记序号校验、无法获取当前纳税人信息：" + djxh);
            return new Result(-1, "无法根据登记序号获取当前绑定纳税人信息、无法进行自检计算" , null);
        }*/
        //2：如果请求重复报告编号首先删除已经计算过此报告编号的业务数据
        hZjbgZbyxjgbMapper.deleteByBgbh(bgbh);
        hZjbgMxMapper.deleteByBgbh(bgbh);
        hZjbgMapper.deleteByBgbh(bgbh);
        //3：组装函数取数方法的参数
        Map<String, Object> params = new HashMap<>();
        params.put("I_ZZDAH", djxh);
        params.put("I_QJQ", format.parse(zjqjq));
        params.put("I_QJZ", format.parse(zjqjz));
        //4：从会标库表获取模型下需要计算的指标编号
        List<HZbk> zbkList = hZbkMapper.queryZbkByMxId(mxbh);
        //5:遍历指标进行指标计算及风险规则计算
        for (HZbk zbk : zbkList) {
            //记录指标开始计算时间
            long start = System.currentTimeMillis();
            //5.1注释掉从本地配置取对应指标计算数据项、改用从数据库配置取数
            //String[] functionNames = YsgjZbConstants.getZbFunctionName(zbk.getZbbh());
            List<HZbJssjx> hZbkJssj = hZbkJssjxMapper.queryZbJssjx(zbk.getZbbh());
            //首先看指标适用不适用、如果不适用直接跳出
            if (hZbkJssj.size() <= 0) {
                continue;
            }
            //5.2如果没有配置计算取数项跳出计算下一指标
            long syfw_bs = 0;
            String syfw_sql = null;
            long gzyqbh = 0;
            String syfw = zbk.getSyfw();
            gzyqbh = zbk.getGwfx();
            //适应范围
            if (syfw.equals("1")) {
                syfw_sql = syfw.replace("#i_djzzdah#", djxh).replace("#date_qjq#", "'" + zjqjq + "'").replace("#date_qjz#", "'" + zjqjz + "'");
                syfw_bs = hZbkMapper.execusql(syfw_sql);
            }
            //当此指标没有适用范围或者根据适用范围按断适用时计算否则直接不计算
            if ((syfw.equals("0") || syfw_bs > 0)) {
                //5.3通过模型编号和指标编号在模型与数据关系表中获取配置的指标类型
                XtFxmxSjgx xtFxmxSjgx = new XtFxmxSjgx();
                xtFxmxSjgx.setBsblag("0");
                xtFxmxSjgx.setYszbbh(zbk.getZbbh());
                xtFxmxSjgx.setMxuuid(mxbh);
                XtFxmxSjgx findExist = xtFxmxSjgxMapper.selectOne(new QueryWrapper<XtFxmxSjgx>(xtFxmxSjgx));
                //5.4定义指标计算日志、指标计算结果及明细存储对象、及规则输入返回对象
                HZjbgLog log = new HZjbgLog();
                //5.4.0 明细lixt
                List<HZjbgMx> listMx = new ArrayList<>();
                //5.4.0.1 定义存放指标计算需要的参数
                HZjbgMx mx = new HZjbgMx();
                HZjbgZbyxjgb zjbgZbyxjgb = new HZjbgZbyxjgb();
                DataItem dataItem = new DataItem();
                ZbJgAndFx zbJgAndFx = new ZbJgAndFx();
                //5.4.1参数对象定义要计算的指标编号
                dataItem.setZbbh(zbk.getZbbh());
                //6:对取到的指标计算项进行计算
                for (int i = 0; i < hZbkJssj.size(); i++) {
                    //6.1获取到计算的方法函数名
                    String functionName = hZbkJssj.get(i).getFfmc();
                    String qslx = hZbkJssj.get(i).getQslx();
                    //6.2如果是0默认就是函数方法取数
                    if (qslx.equals("0")) {
                        //6.2.1函数的指标数据计算项计算
                        Double data = null;
                        if (functionName.endsWith("-基期")) {
                            params.put("functionName", functionName.substring(0, functionName.length() - 3));
                            hZbkMapper.queryDataItemToBasePeriod(params);
                            dataItem.setFile(functionName, params.get("Data") == null ? null : Double.parseDouble(params.get("Data").toString()));
                            mx.setMx(i, params.get("Data") == null ? null : params.get("Data").toString());
                            params.put("Data", null);
                        } else if (functionName.endsWith("-上期")) {
                            params.put("functionName", functionName.substring(0, functionName.length() - 3));
                            hZbkMapper.queryDataItemToThePeriod(params);
                            dataItem.setFile(functionName, params.get("Data") == null ? null : Double.parseDouble(params.get("Data").toString()));
                            mx.setMx(i, params.get("Data") == null ? null : params.get("Data").toString());
                            params.put("Data", null);
                        } else {
                            //params.put("functionName", functionName);
                            //hZbkMapper.queryDataItem(params);
                            //dataItem.setFile(functionName, params.get("Data") == null ? null : Double.parseDouble(params.get("Data").toString()));
                            //mx.setMx(i, params.get("Data") == null ? null : params.get("Data").toString());
                            //params.put("Data", null);
                            Double zbData = hiveFunctionFetch.fetchData(functionName, djxh, zjqjq, zjqjz);
                            dataItem.setFile(functionName, zbData);
                            mx.setMx(i, zbData == null ? null : zbData.toString());
                        }

                    } else {
                        //6.2.2否则类型为1就是java方法取数、统一将获取到的结果集放到map中
                        List<HZjbgMx> ffjsx = getFfjsx(zjqjq, zjqjz, zbk.getZbbh(), djxh);
                        dataItem.setList(ffjsx);
                    }
                }
                FactHandle dataItemFactHandle = kieSession.insert(dataItem);
                FactHandle zbJgAndFxFactHandle = kieSession.insert(zbJgAndFx);
                FactHandle zjbgMxFactHandle = kieSession.insert(listMx);
                kieSession.fireAllRules((int) gzyqbh);
                kieSession.delete(dataItemFactHandle);
                kieSession.delete(zbJgAndFxFactHandle);
                kieSession.delete(zjbgMxFactHandle);
                System.out.println(dataItem.getZbbh() + "返回指标风险 : " + zbJgAndFx);
                System.out.println(dataItem.getZbbh() + "返回明细数: " + listMx.size());
                String uuid = UUID.randomUUID().toString().replace("-", "");
                String mxId = UUID.randomUUID().toString().replace("-", "");
                //结果表 公共部分
                zjbgZbyxjgb.setId(uuid);
                zjbgZbyxjgb.setBgbh(bgbh);
                zjbgZbyxjgb.setZbbh(zbk.getZbbh());
                zjbgZbyxjgb.setZbmc(zbk.getZbmc());
                zjbgZbyxjgb.setYwsjq(format.parse(zjqjq));
                zjbgZbyxjgb.setYwsjz(format.parse(zjqjz));
                zjbgZbyxjgb.setCreateDate(new Date());
                zjbgZbyxjgb.setModifiDate(new Date());
                zjbgZbyxjgb.setBsblag("0");
                zjbgZbyxjgb.setDjxh(djxh);
                zjbgZbyxjgb.setQymc(qymc);
                zjbgZbyxjgb.setSjlxDm(findExist.getGlsjLx());
                zjbgZbyxjgb.setSjwzBz(zbJgAndFx.getSjwzBz());
                zjbgZbyxjgb.setFxdj(zbJgAndFx.getFxdj());
                //结果表
                zjbgZbyxjgb.setZbjg(zbJgAndFx.getZbjg());
                zjbgZbyxjgb.setZbjgSj(zbJgAndFx.getZbjgSj());
                zjbgZbyxjgb.setZbjgSj1(zbJgAndFx.getZbjgSj1());
                zjbgZbyxjgb.setZbjgSj2(zbJgAndFx.getZbjgSj2());
                zjbgZbyxjgb.setZbjgSj3(zbJgAndFx.getZbjgSj3());
                zjbgZbyxjgb.setZbjgSj4(zbJgAndFx.getZbjgSj4());
                zjbgZbyxjgb.setZbjgSj5(zbJgAndFx.getZbjgSj5());
                zjbgZbyxjgb.setZbjgSj6(zbJgAndFx.getZbjgSj6());
                zjbgZbyxjgb.setZbjgSj7(zbJgAndFx.getZbjgSj7());
                zjbgZbyxjgb.setZbjgSj8(zbJgAndFx.getZbjgSj8());
                //数据是否完整\存入明细信息
                if (zbJgAndFx.getSjwzBz().equals("1") && zbJgAndFx.getZbjg().equals("Y")) {
                    //mx 明细表
                    for (int j = 0; j < listMx.size(); j++) {
                        //明细公共数据
                        mx.setId(mxId);
                        mx.setYxid(uuid);
                        mx.setBgbh(bgbh);
                        mx.setZbbh(zbk.getZbbh());
                        mx.setDjxh(djxh);
                        mx.setYwsjq(format.parse(zjqjq));
                        mx.setYwsjz(format.parse(zjqjz));
                        mx.setCreateDate(new Date());
                        mx.setModifiDate(new Date());
                        mx.setBsblag("0");
                        //明细属性值
                        mx.setSx1(listMx.get(j).getSx1());
                        mx.setSx2(listMx.get(j).getSx2());
                        mx.setSx3(listMx.get(j).getSx3());
                        mx.setSx4(listMx.get(j).getSx4());
                        mx.setSx5(listMx.get(j).getSx5());
                        mx.setSx6(listMx.get(j).getSx6());
                        mx.setSx7(listMx.get(j).getSx7());
                        mx.setSx8(listMx.get(j).getSx8());
                        mx.setSx9(listMx.get(j).getSx9());
                        mx.setSx10(listMx.get(j).getSx10());
                        hZjbgMxMapper.insert(mx);
                    }
                }
                hZjbgZbyxjgbMapper.insert(zjbgZbyxjgb);
                //log 信息表保存
                log.setId(uuid);
                log.setDjxh(djxh);
                log.setBgbh(bgbh);
                log.setZbbh(zbk.getZbbh());
                log.setZbmc(zbk.getZbmc());
                log.setStartTime(new Date());
                log.setEndTime(new Date());
                long end = System.currentTimeMillis();
                log.setDuration(end - start);
                log.setRetCode("0");
                log.setRetMessage("成功");
                log.setCreateDate(new Date());
                log.setModifiDate(new Date());
                log.setBsblag("0");
                hZjbgLogMapper.insert(log);
            }
        }
        //7：汇总自检报告表中的信息指标风险运行情况
        Long runZbCount = hZjbgZbyxjgbMapper.queryRunZbCount(djxh, bgbh, mxbh);
        Long fxZbCount = hZjbgZbyxjgbMapper.queryFxZbCount(djxh, bgbh, mxbh);
        Long dataItemCount = hZjbgZbyxjgbMapper.queryQueDataItemCount(djxh, bgbh, mxbh);
        HZjbg zjbg = new HZjbg();
        zjbg.setId(UUID.randomUUID().toString().replace("-", ""));
        zjbg.setMxbh(mxbh);
        zjbg.setBgbh(bgbh);
        zjbg.setDjxh(djxh);
        zjbg.setShxydm(nsrsbh);
        zjbg.setQymc(qymc);
        zjbg.setZjqjq(format.parse(zjqjq));
        zjbg.setZjqjz(format.parse(zjqjz));
        zjbg.setYxzjzbs(runZbCount);
        zjbg.setQsjzbSl(dataItemCount);
        zjbg.setXzbzbsjBs("N");
        zjbg.setFxsl(fxZbCount);
        zjbg.setWfxx(runZbCount - fxZbCount);
        zjbg.setCreateDate(new Date());
        zjbg.setModifiDate(new Date());
        zjbg.setBsblag("0");
        hZjbgMapper.insert(zjbg);
        return Result.success();
    }


//    @Override
//    @Transactional(transactionManager = "mysqlTransactionManager")
//    public Result cswsZjbgForSC(String  bgbh, String djxh, String qymc,String nsrsbh,String zjqjq, String zjqjz, String mxbh) throws ParseException {
//        //1:日期格式化
//        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
//       /* //获取企业基本信息
//        HRelationEnterprise relationEnterprise = hZjbgMapper.queryHRelationEnterprise(djxh);
//        if(relationEnterprise==null){
//            log.error("登记序号校验、无法获取当前纳税人信息：" + djxh);
//            return new Result(-1, "无法根据登记序号获取当前绑定纳税人信息、无法进行自检计算" , null);
//        }*/
//        //2：如果请求重复报告编号首先删除已经计算过此报告编号的业务数据
//        hZjbgZbyxjgbMapper.deleteByBgbh(bgbh);
//        hZjbgMxMapper.deleteByBgbh(bgbh);
//        hZjbgMapper.deleteByBgbh(bgbh);
//        //3：组装函数取数方法的参数
//        Map<String, Object> params = new HashMap<>();
//        params.put("I_ZZDAH", djxh);
//        params.put("I_QJQ", format.parse(zjqjq));
//        params.put("I_QJZ", format.parse(zjqjz));
//        //4：从会标库表获取模型下需要计算的指标编号
//        List<HZbk> zbkList = hZbkMapper.queryZbkByMxId(mxbh);
//        //5:遍历指标进行指标计算及风险规则计算
//        for (HZbk zbk : zbkList) {
//            //记录指标开始计算时间
//            long start = System.currentTimeMillis();
//            //5.1注释掉从本地配置取对应指标计算数据项、改用从数据库配置取数
//            //String[] functionNames = YsgjZbConstants.getZbFunctionName(zbk.getZbbh());
//            List<HZbJssjx> hZbkJssj = hZbkJssjxMapper.queryZbJssjx(zbk.getZbbh());
//            //首先看指标适用不适用、如果不适用直接跳出
//            if (hZbkJssj.size() <= 0) {
//                continue;
//            }
//            //5.2如果没有配置计算取数项跳出计算下一指标
//            long syfw_bs = 0;
//            String syfw_sql = null;
//            long gzyqbh=0;
//            String syfw = zbk.getSyfw();
//            gzyqbh=zbk.getGwfx();
//            //适应范围
//            if (syfw.equals("1")) {
//                syfw_sql = syfw.replace("#i_djzzdah#", djxh).replace("#date_qjq#", "'" + zjqjq + "'").replace("#date_qjz#", "'" + zjqjz + "'");
//                syfw_bs = hZbkMapper.execusql(syfw_sql);
//            }
//            //当此指标没有适用范围或者根据适用范围按断适用时计算否则直接不计算
//            if ((syfw.equals("0") || syfw_bs>0)){
//                //5.3通过模型编号和指标编号在模型与数据关系表中获取配置的指标类型
//                XtFxmxSjgx xtFxmxSjgx = new XtFxmxSjgx();
//                xtFxmxSjgx.setBsblag("0");
//                xtFxmxSjgx.setYszbbh(zbk.getZbbh());
//                xtFxmxSjgx.setMxuuid(mxbh);
//                XtFxmxSjgx findExist = xtFxmxSjgxMapper.selectOne(new QueryWrapper<XtFxmxSjgx>(xtFxmxSjgx));
//                //5.4定义指标计算日志、指标计算结果及明细存储对象、及规则输入返回对象
//                HZjbgLog log = new HZjbgLog();
//                //5.4.0 明细lixt
//                List<HZjbgMx> listMx = new ArrayList<>();
//                //5.4.0.1 定义存放指标计算需要的参数
//                HZjbgMx mx = new HZjbgMx();
//                HZjbgZbyxjgb zjbgZbyxjgb = new HZjbgZbyxjgb();
//                DataItem dataItem = new DataItem();
//                ZbJgAndFx zbJgAndFx = new ZbJgAndFx();
//                //5.4.1参数对象定义要计算的指标编号
//                dataItem.setZbbh(zbk.getZbbh());
//                //6:对取到的指标计算项进行计算
//                for (int i = 0; i < hZbkJssj.size(); i++) {
//                    //6.1获取到计算的方法函数名
//                    String functionName = hZbkJssj.get(i).getFfmc();
//                    String qslx = hZbkJssj.get(i).getQslx();
//                    //6.2如果是0默认就是函数方法取数
//                    if (qslx.equals("0")) {
//                        //6.2.1函数的指标数据计算项计算
//                        Double data = null;
//                        if (functionName.endsWith("-基期")) {
//                            params.put("functionName", functionName.substring(0, functionName.length() - 3));
//                            hZbkMapper.queryDataItemToBasePeriod(params);
//                            dataItem.setFile(functionName, params.get("Data") == null ? null : Double.parseDouble(params.get("Data").toString()));
//                            mx.setMx(i, params.get("Data") == null ? null : params.get("Data").toString());
//                            params.put("Data", null);
//                        } else if (functionName.endsWith("-上期")) {
//                            params.put("functionName", functionName.substring(0, functionName.length() - 3));
//                            hZbkMapper.queryDataItemToThePeriod(params);
//                            dataItem.setFile(functionName, params.get("Data") == null ? null : Double.parseDouble(params.get("Data").toString()));
//                            mx.setMx(i, params.get("Data") == null ? null : params.get("Data").toString());
//                            params.put("Data", null);
//                        } else {
//                            params.put("functionName", functionName);
//                            hZbkMapper.queryDataItem(params);
//                            dataItem.setFile(functionName, params.get("Data") == null ? null : Double.parseDouble(params.get("Data").toString()));
//                            mx.setMx(i, params.get("Data") == null ? null : params.get("Data").toString());
//                            params.put("Data", null);
//                        }
//
//                    } else {
//                        //6.2.2否则类型为1就是java方法取数、统一将获取到的结果集放到map中
//                        List<HZjbgMx> ffjsx = getFfjsx(zjqjq, zjqjz, zbk.getZbbh(),djxh);
//                        dataItem.setList(ffjsx);
//                    }
//                }
//                FactHandle dataItemFactHandle = kieSession.insert(dataItem);
//                FactHandle zbJgAndFxFactHandle = kieSession.insert(zbJgAndFx);
//                FactHandle zjbgMxFactHandle = kieSession.insert(listMx);
//                kieSession.fireAllRules((int) gzyqbh);
//                kieSession.delete(dataItemFactHandle);
//                kieSession.delete(zbJgAndFxFactHandle);
//                kieSession.delete(zjbgMxFactHandle);
//                System.out.println(dataItem.getZbbh() + "返回指标风险 : " + zbJgAndFx);
//                System.out.println(dataItem.getZbbh() + "返回明细数: " + listMx.size());
//                String uuid = UUID.randomUUID().toString().replace("-", "");
//                String mxId = UUID.randomUUID().toString().replace("-", "");
//                //结果表 公共部分
//                zjbgZbyxjgb.setId(uuid);
//                zjbgZbyxjgb.setBgbh(bgbh);
//                zjbgZbyxjgb.setZbbh(zbk.getZbbh());
//                zjbgZbyxjgb.setZbmc(zbk.getZbmc());
//                zjbgZbyxjgb.setYwsjq(format.parse(zjqjq));
//                zjbgZbyxjgb.setYwsjz(format.parse(zjqjz));
//                zjbgZbyxjgb.setCreateDate(new Date());
//                zjbgZbyxjgb.setModifiDate(new Date());
//                zjbgZbyxjgb.setBsblag("0");
//                zjbgZbyxjgb.setDjxh(djxh);
//                zjbgZbyxjgb.setQymc(qymc);
//                zjbgZbyxjgb.setSjlxDm(findExist.getGlsjLx());
//                zjbgZbyxjgb.setSjwzBz(zbJgAndFx.getSjwzBz());
//                zjbgZbyxjgb.setFxdj(zbJgAndFx.getFxdj());
//                //结果表
//                zjbgZbyxjgb.setZbjg(zbJgAndFx.getZbjg());
//                zjbgZbyxjgb.setZbjgSj(zbJgAndFx.getZbjgSj());
//                zjbgZbyxjgb.setZbjgSj1(zbJgAndFx.getZbjgSj1());
//                zjbgZbyxjgb.setZbjgSj2(zbJgAndFx.getZbjgSj2());
//                zjbgZbyxjgb.setZbjgSj3(zbJgAndFx.getZbjgSj3());
//                zjbgZbyxjgb.setZbjgSj4(zbJgAndFx.getZbjgSj4());
//                zjbgZbyxjgb.setZbjgSj5(zbJgAndFx.getZbjgSj5());
//                zjbgZbyxjgb.setZbjgSj6(zbJgAndFx.getZbjgSj6());
//                zjbgZbyxjgb.setZbjgSj7(zbJgAndFx.getZbjgSj7());
//                zjbgZbyxjgb.setZbjgSj8(zbJgAndFx.getZbjgSj8());
//                //数据是否完整\存入明细信息
//                if (zbJgAndFx.getSjwzBz().equals("1")&& zbJgAndFx.getZbjg().equals("Y")) {
//                    //mx 明细表
//                    for(int j = 0; j < listMx.size(); j++){
//                        //明细公共数据
//                        mx.setId(mxId);
//                        mx.setYxid(uuid);
//                        mx.setBgbh(bgbh);
//                        mx.setZbbh(zbk.getZbbh());
//                        mx.setDjxh(djxh);
//                        mx.setYwsjq(format.parse(zjqjq));
//                        mx.setYwsjz(format.parse(zjqjz));
//                        mx.setCreateDate(new Date());
//                        mx.setModifiDate(new Date());
//                        mx.setBsblag("0");
//                        //明细属性值
//                        mx.setSx1(listMx.get(j).getSx1());
//                        mx.setSx2(listMx.get(j).getSx2());
//                        mx.setSx3(listMx.get(j).getSx3());
//                        mx.setSx4(listMx.get(j).getSx4());
//                        mx.setSx5(listMx.get(j).getSx5());
//                        mx.setSx6(listMx.get(j).getSx6());
//                        mx.setSx7(listMx.get(j).getSx7());
//                        mx.setSx8(listMx.get(j).getSx8());
//                        mx.setSx9(listMx.get(j).getSx9());
//                        mx.setSx10(listMx.get(j).getSx10());
//                        hZjbgMxMapper.insert(mx);
//                    }
//                }
//                hZjbgZbyxjgbMapper.insert(zjbgZbyxjgb);
//                //log 信息表保存
//                log.setId(uuid);
//                log.setDjxh(djxh);
//                log.setBgbh(bgbh);
//                log.setZbbh(zbk.getZbbh());
//                log.setZbmc(zbk.getZbmc());
//                log.setStartTime(new Date());
//                log.setEndTime(new Date());
//                long end = System.currentTimeMillis();
//                log.setDuration(end - start);
//                log.setRetCode("0");
//                log.setRetMessage("成功");
//                log.setCreateDate(new Date());
//                log.setModifiDate(new Date());
//                log.setBsblag("0");
//                hZjbgLogMapper.insert(log);
//            }
//        }
//        //7：汇总自检报告表中的信息指标风险运行情况
//        Long runZbCount = hZjbgZbyxjgbMapper.queryRunZbCount(djxh, bgbh, mxbh);
//        Long fxZbCount = hZjbgZbyxjgbMapper.queryFxZbCount(djxh, bgbh, mxbh);
//        Long dataItemCount = hZjbgZbyxjgbMapper.queryQueDataItemCount(djxh, bgbh, mxbh);
//        HZjbg zjbg = new HZjbg();
//        zjbg.setId(UUID.randomUUID().toString().replace("-", ""));
//        zjbg.setMxbh(mxbh);
//        zjbg.setBgbh(bgbh);
//        zjbg.setDjxh(djxh);
//        zjbg.setShxydm(nsrsbh);
//        zjbg.setQymc(qymc);
//        zjbg.setZjqjq(format.parse(zjqjq));
//        zjbg.setZjqjz(format.parse(zjqjz));
//        zjbg.setYxzjzbs(runZbCount);
//        zjbg.setQsjzbSl(dataItemCount);
//        zjbg.setXzbzbsjBs("N");
//        zjbg.setFxsl(fxZbCount);
//        zjbg.setWfxx(runZbCount - fxZbCount);
//        zjbg.setCreateDate(new Date());
//        zjbg.setModifiDate(new Date());
//        zjbg.setBsblag("0");
//        hZjbgMapper.insert(zjbg);
//        return Result.success();
//    }


    @Transactional(transactionManager = "oracleTransactionManager")
    public List<HZjbgMx> getFfjsx(String qjq, String qjz, String zbbh, String djxh) {
        List<HZjbgMx> classes = ysgjZbConstants.JQs(qjq, qjz, zbbh, djxh);
        return classes;
    }


}
