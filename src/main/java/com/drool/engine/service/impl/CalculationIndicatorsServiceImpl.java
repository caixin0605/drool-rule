package com.drool.engine.service.impl;

import com.drool.engine.entity.*;
import com.drool.engine.mapper.oracle.Fp0001Mapper;
import com.drool.engine.service.CalculationIndicatorsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;
import java.util.*;

/**
 * @author 菜心
 * @date 2020/8/18
 */
@Service
@Transactional(transactionManager = "oracleTransactionManager")
public class CalculationIndicatorsServiceImpl implements CalculationIndicatorsService {

    @Autowired
    private Fp0001Mapper fp0001Mapper;

    //获取发票001指标计算的数据项结果集
    @Override
    @Transactional(transactionManager = "oracleTransactionManager")
    public List<HZjbgMx> getFp0001Sjx(String qjq, String qjz, String  djxh) {
        return fp0001Mapper.getFp0001Sjx(qjq,qjz,djxh);
    }

}
