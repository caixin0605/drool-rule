package com.drool.engine.service;

import com.drool.engine.entity.HZjbgMx;
import com.drool.engine.entity.Result;

import java.text.ParseException;
import java.util.List;

/**
 * @author 菜心
 * @date 2020/8/18
 */

public interface CalculationIndicatorsService {
    List<HZjbgMx> getFp0001Sjx(String qjq, String qjz, String  djxh);
}
