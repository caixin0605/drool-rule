package com.drool.engine.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.Date;

/**
 * @author 菜心
 * @date 2020/8/19
 * 模型数据关系表
 */
@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
@TableName(value = "xt_fxmx_sjgx")
public class XtFxmxSjgx {
    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.UUID)
    private String id;

    /**
     * 指标结果明细ID
     */
    @TableField(value = "mxuuid")
    private String mxuuid;

    /**
     * 关联数据类型:1-指标，2-元数据
     */
    @TableField(value = "glsj_lx")
    private String glsjLx;

    /**
     * 风险标记，１风险指标，２数据值展示
     */
    @TableField(value = "fx_bj")
    private String fxBj;
    /**
     * 元素或指标编号
     */
    @TableField(value = "yszbbh")
    private String yszbbh;

    /**
     * 序号
     */
    @TableField(value = "xh")
    private Integer xh;

    /**
     * 风险判断标准值
     */
    @TableField(value = "bzz")
    private String bzz;

    /**
     * 参考标准值
     */
    @TableField(value = "ckbzz")
    private String ckbzz;

    /**
     * 标准值来源：0，无标准值，１-原有判断规则，２-第三方表
     */
    @TableField(value = "bzz_ly")
    private String bzzLy;

    /**
     * 等级,1-直接判定高，2-规则判断,3-直接判断中，4-直接判断低，5-直接判断风险提醒
     */
    @TableField(value = "dj")
    private String dj;

    /**
     * 规则说明
     */
    @TableField(value = "gzsm")
    private String gzsm;

    /**
     * 1-存储过程，5-动态SQL
     */
    @TableField(value = "gzwj_lx")
    private String gzwjLx;

    /**
     * 取数规则文件,规则在SQL
     */
    @TableField(value = "gzwj")
    private String gzwj;

    /**
     * 取数规则文件,规则在第三方表
     */
    @TableField(value = "gzwj_gzb")
    private String gzwjGzb;

    /**
     * 等级规则文件
     */
    @TableField(value = "djgzwj")
    private String djgzwj;

//    /**
//     * 创建时间
//     */
//    @TableField(value = "create_date")
//    private Date createDate;
//
//    /**
//     * 修改时间
//     */
//    @TableField(value = "modifi_date")
//    private Date modifi_date;

    /**
     * 删除标志
     */
    @TableField(value = "bsblag")
    private String bsblag;


}
