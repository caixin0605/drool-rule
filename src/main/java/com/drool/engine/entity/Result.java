package com.drool.engine.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;

/**
 * 统一封装API返回结果
 *
 * @author liudongming
 * @Time 2018-06-03
 */
@Data
@ToString
public class Result implements Serializable {

    private static final long serialVersionUID = -235247802008089764L;

    //返回值代码，成功：0，错误：非0
    private int code;
    //返回值代码描述，0：成功，非0：参见返回值代码表"
    private String message;
    //返回的数据
    private Object data;

    public Result() {
    }

    public Result(int code, String message, Object date) {
        this.code = code;
        this.message = message;
        this.data = date;
    }

    public static Result success() {
        Result result = new Result();
        result.setResultCode(ResultCode.SUCCESS);
        return result;
    }

    public static Result success(Object data) {
        Result result = new Result();
        result.setResultCode(ResultCode.SUCCESS);
        result.setData(data);
        return result;
    }

    public static Result failure(ResultCode resultCode) {
        Result result = new Result();
        result.setResultCode(resultCode);
        return result;
    }

    public static Result failure(ResultCode resultCode, Object data) {
        Result result = new Result();
        result.setResultCode(resultCode);
        result.setData(data);
        return result;
    }

    public void setResultCode(ResultCode code) {
        this.code = code.code();
        this.message = code.message();
    }

}
