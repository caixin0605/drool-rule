package com.drool.engine.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.io.Serializable;
import java.util.Date;

/**
 * 指标结果表 HZjbgZbyxjgb
 *
 * @author xincai
 * @date 2020-08-18
 */
@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
@TableName(value = "h_zjbg_zbyxjgb")
public class HZjbgZbyxjgb implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.UUID)
    private String id;

    /**
     * $column.columnComment
     */
    @TableField(value = "bgbh")
    private String bgbh;

    /**
     * 登记序号
     */
    @TableField(value = "djxh")
    private String djxh;

    /**
     * 企业名称
     */
    @TableField(value = "qymc")
    private String qymc;

    /**
     * 数据类型；1；指标，2-数据项
     */
    @TableField(value = "sjlx_dm")
    private String sjlxDm;

    /**
     * 指标编号或元数据编号
     */
    @TableField(value = "zbbh")
    private String zbbh;

    /**
     * 指标名称或数据项名称
     */
    @TableField(value = "zbmc")
    private String zbmc;

    /**
     * 序号
     */
    @TableField(value = "xh")
    private Long xh;

    /**
     * 源数据业务区间起
     */
    @TableField(value = "ywsjq")
    private Date ywsjq;

    /**
     * 源数据业务区间止
     */
    @TableField(value = "ywsjz")
    private Date ywsjz;

    /**
     * 风险等级,1-低，２中，３－高，４－提示
     */
    @TableField(value = "fxdj")
    private String fxdj;

    /**
     * 指标运行结果：无风险N，有风险Y
     */
    @TableField(value = "zbjg")
    private String zbjg;

    /**
     * 指标运行结果值
     */
    @TableField(value = "zbjg_sj")
    private String zbjgSj;

    /**
     * 指标运行结果值1
     */
    @TableField(value = "zbjg_sj1")
    private String zbjgSj1;

    /**
     * 指标运行结果值2
     */
    @TableField(value = "zbjg_sj2")
    private String zbjgSj2;

    /**
     * 指标运行结果值3
     */
    @TableField(value = "zbjg_sj3")
    private String zbjgSj3;

    /**
     * 指标运行结果值4
     */
    @TableField(value = "zbjg_sj4")
    private String zbjgSj4;

    /**
     * 指标运行结果值5
     */
    @TableField(value = "zbjg_sj5")
    private String zbjgSj5;

    /**
     * 指标运行结果值6
     */
    @TableField(value = "zbjg_sj6")
    private String zbjgSj6;

    /**
     * 指标运行结果值7
     */
    @TableField(value = "zbjg_sj7")
    private String zbjgSj7;

    /**
     * 指标运行结果值8
     */
    @TableField(value = "zbjg_sj8")
    private String zbjgSj8;

    /**
     * 指标运行结果值9
     */
    @TableField(value = "zbjg_sj9")
    private String zbjgSj9;

    /**
     * 指标运行结果值10
     */
    @TableField(value = "zbjg_sj10")
    private String zbjgSj10;

    /**
     * 1-完整
     * 2-不完整
     */
    @TableField(value = "sjwz_bz")
    private String sjwzBz;

    /**
     * 创建时间
     */
    @TableField(value = "create_date")
    private Date createDate;

    /**
     * 修改时间
     */
    @TableField(value = "modifi_date")
    private Date modifiDate;

    /**
     * 状态标识
     */
    @TableField(value = "bsblag")
    private String bsblag;

    /**
     * 风险点编码
     */
    @TableField(value = "fxd_bm")
    private String fxdBm;

    /**
     * 目录编码
     */
    @TableField(value = "fl_bm")
    private String flBm;

    /**
     * 上级id
     */
    @TableField(value = "sj_id")
    private String sjId;

}
