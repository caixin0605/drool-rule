package com.drool.engine.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * 指标计算数据项信息表
 * @author fly
 * @date 2020-08-21
 */
@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
@TableName(value = "h_zb_jssjx")
public class HZbJssjx implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.UUID)
    private String id;

    /**
     * 指标名称
     */
    @TableField(value = "zbmc")
    private String zbmc;

    /**
     * 指标编号
     */
    @TableField(value = "zbbh")
    private String zbbh;

    /**
     * 方法名称
     */
    @TableField(value = "ffmc")
    private String ffmc;

    /**
     * 取数类型
     */
    @TableField(value = "qslx")
    private String qslx;

    /**
     * 数据标识
     */
    @TableField(value = "sjbs")
    private String sjbs;

    /**
     * 数据名称
     */
    @TableField(value = "sjmc")
    private String sjmc;

    /**
     * 删除标识
     */
    @TableField(value = "bsflag")
    private String bsflag;



}
