package com.drool.engine.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author 菜心
 * @date 2020/8/19
 * 企业关联信息表:企业关联信息表
 */
@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class HRelationEnterprise {
    private String nsrmc;
    private String naShiRenShiMingCheng;
    private String nsrsbh;
}
