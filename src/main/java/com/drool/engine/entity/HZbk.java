package com.drool.engine.entity;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;

/**
 * @author: ZhaiYongJie
 * @date: 2018/10/17
 */
@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
@TableName(value = "h_zbk")
public class HZbk implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 指标编号
     */
    @TableField(value = "zbbh")
    private String zbbh;

    /**
     * 指标名称
     */
    @TableField(value = "zbmc")
    private String zbmc;

    /**
     * 规则文件
     */
    @TableField(value = "gzwj")
    private String gzwj;

    /**
     * 适用范围
     */
    @TableField(value = "syfw")
    private String syfw;

    /**
     * 重点指标标记
     */
    @TableField(value = "gwfx")
    private int gwfx;

    public int getGwfx() {
        return gwfx;
    }

    public void setGwfx(int gwfx) {
        this.gwfx = gwfx;
    }

    public String getSyfw() {
        return syfw;
    }

    public void setSyfw(String syfw) {
        this.syfw = syfw;
    }

    public String getZbbh() {
        return zbbh;
    }

    public void setZbbh(String zbbh) {
        this.zbbh = zbbh;
    }

    public String getZbmc() {
        return zbmc;
    }

    public void setZbmc(String zbmc) {
        this.zbmc = zbmc;
    }

    public String getGzwj() {
        return gzwj;
    }

    public void setGzwj(String gzwj) {
        this.gzwj = gzwj;
    }

}
