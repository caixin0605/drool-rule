package com.drool.engine.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author 菜心
 * @date 2020/8/18
 * 指标运算结果与风险等级
 */
@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class ZbJgAndFx {
    /**
     * 1-完整  2-不完整
     */
    private String sjwzBz;
    /**
     * 指标运行结果：无风险N，有风险Y
     */
    private String zbjg;
    /**
     * 风险等级,1-低，２中，３－高，４－提示
     */
    private String fxdj;
    /**
     * 指标运行结果值
     */
    private String zbjgSj;
    /**
     * 指标运行结果值1
     */
    private String zbjgSj1;
    /**
     * 指标运行结果值2
     */
    private String zbjgSj2;
    /**
     * 指标运行结果值3
     */
    private String zbjgSj3;
    /**
     * 指标运行结果值4
     */
    private String zbjgSj4;
    /**
     * 指标运行结果值5
     */
    private String zbjgSj5;
    /**
     * 指标运行结果值6
     */
    private String zbjgSj6;
    /**
     * 指标运行结果值7
     */
    private String zbjgSj7;
    /**
     * 指标运行结果值8
     */
    private String zbjgSj8;

}
