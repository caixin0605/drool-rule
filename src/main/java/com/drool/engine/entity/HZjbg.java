package com.drool.engine.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.io.Serializable;
import java.util.Date;

/**
 * 报告(模型)运行结果对象 h_zjbg
 *
 * @author ruoyi
 * @date 2020-08-18
 */
@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
@TableName(value = "h_zjbg")
public class HZjbg implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.UUID)
    private String id;

    /**
     * 模型编号
     */
    @TableField(value = "mxbh")
    private String mxbh;

    /**
     * $column.columnComment
     */
    @TableField(value = "bgbh")
    private String bgbh;

    /**
     * 登记序号
     */
    @TableField(value = "djxh")
    private String djxh;

    /**
     * 社会信用代码
     */
    @TableField(value = "shxydm")
    private String shxydm;

    /**
     * 企业名称
     */
    @TableField(value = "qymc")
    private String qymc;

    /**
     * 自检业务区间起
     */
    @TableField(value = "zjqjq")
    private Date zjqjq;

    /**
     * 自检业务区间止
     */
    @TableField(value = "zjqjz")
    private Date zjqjz;

    /**
     * 风险结果描述
     */
    @TableField(value = "fxjgms")
    private String fxjgms;

    /**
     * 模型得分
     */
    @TableField(value = "mxdf")
    private Double mxdf;

    /**
     * 风险等级
     */
    @TableField(value = "fxdj")
    private Long fxdj;

    /**
     * 总运行风险指标数(风险指标+无风险指标+缺数据指标)
     */
    @TableField(value = "yxzjzbs")
    private Long yxzjzbs;

    /**
     * 缺数据项指标数（风险指标）
     */
    @TableField(value = "qsjzb_sl")
    private Long qsjzbSl;

    /**
     * Y,已具有，N未具备
     */
    @TableField(value = "xzbzbsj_bs")
    private String xzbzbsjBs;

    /**
     * 风险指标数量（风险指标）
     */
    @TableField(value = "fxsl")
    private Long fxsl;

    /**
     * 无风险指标数量（风险指标）
     */
    @TableField(value = "wfxx")
    private Long wfxx;

    /**
     * 重点关注风险项
     */
    @TableField(value = "fxx_zdgz")
    private Long fxxZdgz;

    /**
     * 提示关注风险项
     */
    @TableField(value = "fxx_ywts")
    private Long fxxYwts;

    /**
     * 一般关注风险项
     */
    @TableField(value = "fxx_ybgz")
    private Long fxxYbgz;

    /**
     * 查补处罚分险项
     */
    @TableField(value = "fxx_cbcf")
    private Long fxxCbcf;

    /**
     * 是否阅读
     */
    @TableField(value = "sfyd")
    private String sfyd;

    /**
     * 发送时间
     */
    @TableField(value = "fssj")
    private Date fssj;

    /**
     * 创建时间
     */
    @TableField(value = "create_date")
    private Date createDate;

    /**
     * 修改时间
     */
    @TableField(value = "modifi_date")
    private Date modifiDate;

    /**
     * 状态标识
     */
    @TableField(value = "bsblag")
    private String bsblag;

}
