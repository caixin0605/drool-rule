package com.drool.engine.entity;

/**
 * 统一封装API返回结果代码
 *
 * @author liudongming
 * @Time 2018-06-03
 */
public enum ResultCode {

    SUCCESS(0, "成功"),

    /*HTTP错误直接返回其代码和信息*/

    SYSTEM_URL_ERROR(-1, "严重错误：URL返回空值！"),
    DATAACQU_FILE_VOLID_FAILED(10000 ,"数据解析：版本及数据校验错误！"),
    /* 参数错误：10001-19999 */
    PARAM_IS_INVALID(10001, "参数无效"),
    PARAM_IS_BLANK(10002, "参数为空"),
    PARAM_TYPE_BIND_ERROR(10003, "参数类型错误"),
    PARAM_NOT_COMPLETE(10004, "参数缺失"),
    PARAM_NOT_UPPORTEDENCODING(10005, "不支持的参数转换字符集"),






    /*
    * 用户错误：20001-29999
    * 用户注册错误：20100-20199
    * 用户登录错误：20200-20299
    * 用户短信错误：20300-20399
    * 用户微信错误：20400-20499
    * 用户第三方错误：20500-20599
    *
    * */
    USER_NOT_LOGGED_IN(20001, "用户未登录"),
    USER_NOT_EXIST(20002, "账号不存在或密码错误"),
    USER_ACCOUNT_FORBIDDEN(20003, "账号已被禁用"),
    USER_HAS_EXISTED(20004, "用户已存在"),
    USER_NUMBER_OVER_ONE(20005, "用户账号有重复"),
    USER_ACCOUNT_VALIDE_FAILE(20006, "验证用户信息失败"),
    USER_ACCOUNT_SUBSCRIBE_FAILE(20007, "请先关注公众号"),
    USER_ACCOUNT_SAVE_FAILE(20008, "关联用户信息失败"),
    USER_ACCOUNT_USERINFO_FAILE(20009, "未绑定账户信息"),
    USER_ACCOUNT_INFO_FAILE(20010, "微信公众号账户信息为空"),
    USER_ACCOUNT_REMOTEINFO_FAILE(20011, "绑定账户功能：：获取局端用户信息为空"),
    USER_NOT_EXIST_IN(20012, "用户未注册，请注册之后再登录"),
    USER_PASSWORD_NOT_EXIST_IN(20013, "用户密码为空，请完善信息之后再登录"),
    USER_ACCOUNTINFO_GET_FAILE(20201, "获取用户信息错误"),
    RANGE_DISCONNECTED(20014, "局端未连通"),
    THE_ENTERPRISE_HAS_BEEN_ASSOCIATED(20015, "【已有人关联了该企业，请更换企业进行关联或联系客服提供企业关系证明材料】"),

    USER_SAVEUSER_FAILE(20101, "用户登陆失败"),
    USER_APPCODE_TIMEOUT_FAILE(20102, "该渠道和应用有效时间已过期"),
    USER_CHANNELCODE__FAILE(20103, "通过渠道未获取到公众号配置信息"),


    USER_USEINFO_FAILE(20200, "登录信息错误：渠道和应用未授权,请从新登陆。"),
    USER_USERRELATION_FAILE(20201, "微信用户：通过openid未查到关联账户信息。"),
    USER_LOGINNOTPHONE_FAILE(20202, "虽然是免密用户，但登陆用户名不是手机号，需要重新登录。"),
    USER_BYPHONE_FAILE(20203, "该手机号不是系统用户"),
    USER_USERRELATION_LOGINOUTFAILE(20204, "微信用户：通过openid未查到关联账户信息，退出账户失败。"),
    USER_PHONENULL_FAILE(20205, "免密用户：手机号不能为空，请从新登陆。"),

    USER_QDYY_GET_FAILE(20301, "该渠道和应用未授权"),

    QRCODE_TOKEN_FAILE(20501, "token获取失败"),
    QRCODE_TICKET_FAILE(20502, "获取推广码ticket值为空"),
    QRCODE_TGM_FAILE(20503, "生成推广码失败"),
    QRCODE_HTTP_FAILE(20504, "生成二维码http请求异常"),
    QRCODE_COON_FAILE(20505, "生成二维码，未找到请求头信息"),
    QRCODE_GYSBM_FAILE(20506, "供应商编码不匹配"),


    JWT_CLAIMS_FAILE(20601, "JWT:token不可解码"),
    JWT_TIMEOUT_FAILE(20602, "JWT:token过期"),
    JWT_SX_FAILE(20603, "token刷新失败"),





    /* 业务错误：30001-39999
    *  财税卫士：30100-30199
    *  财税哨兵：30200-30299
    *  财税小二：30300-30399
    *  优惠提醒：30400-30499
    *  企业绑定：30500-30599
    *
    * */
    SPECIFIED_QUESTIONED_USER_NOT_EXIST(30001, "某业务出现问题"),
    SPECIFIED_QUESTIONED_CSXE_FAILE(30002, "获取财税小二业务消息出现问题"),
    SPECIFIED_QUESTIONED_CSXE_UPDATEFAILE(30003, "更新局端财税小二业务数据失败"),
    SPECIFIED_QUESTIONED_CSWS_FAILE(30004, "获取财税卫士业务消息失败"),
    SPECIFIED_QUESTIONED_ZJBG_WAIT_FAILE(30005, "生成自检报告编号异常"),
    SPECIFIED_QUESTIONED_RELATION_FAILE(30006, "获取企业关联用户信息信息失败"),
    SPECIFIED_QUESTIONED_CSWS_SHOW_HOME_FAILE(30007, "财税卫士首页页面时展示异常"),
    SPECIFIED_QUESTIONED_CSSB_CANNOTREPEATSUBSCRIBE(30008, "不能重复订阅"),
    SPECIFIED_QUESTIONED_CSSB_SUBSCRIBE_SUCCESS(30009, "订阅成功"),
    SPECIFIED_QUESTIONED_CSSB_SUBSCRIBE_FAILURE(30010, "订阅失败"),
    SPECIFIED_QUESTIONED_CSWS_BUSINESSRELATION_NULL(30011, "获取关联企业列表为空"),
    SPECIFIED_QUESTIONED_CSWS_WAITLIST_NULL(30012, "财税卫士获取待扫描列表为空"),
    SPECIFIED_CSXE_UPDATEFAILE(30013, "更新财税小二业务数据失败"),
    SPECIFIED_QUESTIONED_CSWS_MINGXI_FAILE(30014, "获取财税卫士自检报告主表失败"),
    SPECIFIED_QUESTIONED_CSWS_ZHUBIAO_FAILE(30015, "获取财税卫士自检报告明细表失败"),
    SPECIFIED_QUESTIONED_CSWS_USERINFO_FAILE(30016, "无关联企业用户信息"),
    SENDMESSAGE_REG_FAILE(30017, "获取手机验证码失败"),
    SENDMESSAGE_NO_COUNT(30018, "手机验证码获取次数超过限制"),
    SENDMESSAGE_UPDATESTATE_FAILE(30019, "更新短信验证码信息失败"),
    SENDMESSAGE_NO_SENDERROR(30020, "调用公共验证码接口工具类，发送证码失败"),
    SENDMESSAGE_YZM_FAILE(30021, "验证码校验失败，在本地信息数据库未查到相关信息"),
    SENDMESSAGE_YZM_TIMEOUT(30022, "验证码已超时，请从新获取"),
    GENERATE_PDF_ZHUBIAO_FAILE(30023, "生成或读取pdf文件失败"),
    SENDMESSAGE_YZM_ERROR(30024, "手机号和验证码错误，请重新发送"),
    SENDMESSAGE_CHECKYZM_ERROR(30025, "校验验证码失败：获取短信配置信息失败，短信账户可能未配置"),
    SENDMESSAGE_USERINFO_FOUNDFAILE(30026, "未找到此账户"),
    SENDMESSAGE_GETYZM_FAILE(30027, "手机号获取验证码失败"),
    LOGLIST_IS_NULL(30028, "获取到的运行日志列表为空"),
    RELATION_NSRXXKZ_GET_FAILE(30029, "获取企业纳税人扩展信息失败"),
    RELATION_NSRMC_GET_FAILE(30030, "获取企业纳税人名称失败"),
    SPECIFIED_QUESTIONED_CSWS_FXTS_FAILE(30031, "获取财税卫士风险提示对象失败"),
    CHECK_SFZ_FAILE(30032, "判断身份证后四位信息失败"),
    CHECK_SMS_FAILE(30033, "验证码错误"),
    YZM_PARAM_IS_INVALID(30034, "请输入验证码"),
    SFZHM_PARAM_IS_INVALID(30035, "请输入身份证号码后四位"),
    NSRSF_PARAM_IS_INVALID(30036, "请选择纳税人身份"),
    DJXH_QY_IS_FAILE(30037, "此企业今天已经在本区间自检过"),
    PARAM_NSRSBH_SHXYDM_IS_BLANK(30038, "请输入纳税人识别号或社会信用代码"),
    PARAM_NSRSBH_IS_BLANK(30039, "请输入纳税人识别号"),
    PARAM_SHXYDM_IS_BLANK(30040, "请输入社会信用代码"),
    SPECIFIED_CHECK_EXIST(30041, "验证失败"),
    ZBKSF_USERNOTJF_FAILE(30042, "当前指标风险为收费指标，如需查看风险详情，请您前往航信会员注册，购买会员服务后即可查看风险明细。"),
    ZBKSF_USERNOTJF_FAILES(30043, "当前指标风险为收费指标，如需查看风险详情，请您前往航信会员注册，购买会员服务后即可查看风险明细。"),
    FILE_CREATE_FAILE(30044, "自检报告生成失败，请重新自检"),
    FILE_CREATE_WAIT(30045, "自检报告正在生成，请稍后查看"),
    FILE_DOWNLOAD_FAILE(30046, "文件下载失败"),
    RELATION_NSRXXKZ_GET_VERIFY_WAY_FAILE(30047, "获取企业验证方式失败"),
    FAILED_TO_SAVE_INFORMATION(30048,"登记保存信息失败"),
    FAILED_TO_MODIFY_INFORMATION(30048,"登记修改信息失败"),
    FILE_ZJ_FAILE(30050, "自检报告调用储存过程失败"),
    SPECIFIED_Q_ZJBG_WAIT_FAILE(30005, "生成自检报告异常"),
    ZJ_PRE_FILE_FAILE(30051, "请上传上个季度的数据"),
    GRXX_TJ_FAILE(30052, "获取个人统计信息失败"),
    FX_TJ_FAILE(30053, "获取风险统计个数失败"),
    XW_QZHZ_FAILE(30054, "汇总风险行为权重失败"),

    UPLOAD_IMAGE_FAILE(30056, "上传图片失败"),
    UPLOAD_FILEDATA_EMPTY(30057, "图片上传失败：文件内容为空"),

    /* 系统错误：40001-49999 */
    SYSTEM_INNER_ERROR(40001, "系统繁忙，请稍后重试"),
    getSystem_RepeatSubmit_ERROR(40002,"请勿重复提交数据"),

    DSF_ZJ_FAILE(40010,"未找到对应的自检报告类型"),

    /* 数据错误：50001-599999 */
    RESULE_DATA_NONE(50001, "数据未找到"),
    DATA_IS_WRONG(50002, "数据有误"),
    DATA_ALREADY_EXISTED(50003, "数据已存在"),
    DATE_ERROR(50503, "自检开始时间不能大于结束时间"),

    COLLECT_EXISTED(50004, "已收藏过，可去我的收藏中查看"),

    NSRXX_INPUTVAILID_EXISTED(50501, "请输入社会信用代码及纳税人识别号"),
    NSRXX_INPUTVAILID_BSRSBHEXISTED(50502, "您已有纳税人识别号，请输入社会信用代码"),
    NSRXX_INPUTVAILID_SHXYDMEXISTED(50503, "您已有社会信用代码，请输入纳税人识别号"),
    NSRXX_INPUTVAILID_NOFOUND(50504, "未查到相关联企业信息"),

    RELATION_SAVE_SUCCESS(51001, "已确认并绑定验证通过。"),
    RELATION_CONFIRM_CHECK(51002, "确认成功，请进行绑定验证"),
    RELATION_HASCONFIRM_FAILECHECK(51003, "当前企业已绑定，未进行验证或者验证未通过，请进行验证。"),
    RELATION_HASRELATION_DOCONFIRM(51004, "当前企业已绑定成功，无需重复绑定。"),
    RELATION_SANERELATION_FAILE(51005, "保存关联企业：根据登记序号获取纳税人信息为空"),
    RELATION_FSD_SUCCESS(51006, "您所绑定的纳税人不是试点地区纳税人，暂不能进行风险自检或自检无结果，可进行操作体验。"),
    RELATION_SANERELATION_QYZJ_FAILE(51007, "企业的纳税人信息为空"),
    RELATION_CONFIRM_QYZJ_CHECK(51008, "企业请进行绑定验证"),
    FX_RELATION_QYZJ_CHECK(51009, "当前用户只能绑定一家企业"),

    NSRXX_DATADJXH_FAILE(51101, "根据登记序号获取纳税人信息失败"),
    NSRXX_NODATADJXH_FAILE(51102, "根据登记序号获取纳税人信息为空"),
    NSRXX_DJZCLXDM_FAILE(51103, "数据错误：登记注册类型代码为空"),
    NSRXX_NSRSBHORSHXYDM_FAILE(51104, "根据纳税人识别号或者社会信用代码获取纳税人信息为空"),
    NSRXX_NSRSBHORSHXYDM_EXCEPTION(51105, "根据纳税人识别号或者社会信用代码获取纳税人信息异常"),
    NSRXX_NSRSBHORSHXYDM(51106, "纳税人识别号输入错误,请重新输入!"),

    YZWT_DJZCLXDM_FAILE(51110, "数据错误：获取验证问题为空"),
    YZWT_SAVE_FAILE(51111, "验证问题保存到本地失败"),
    YZWT_YZWT1_FAILE(51112, "问题1验证失败：不存在此问题"),
    YZWT_YZWT1_DAFAILE(51113, "问题1验证失败：问题答案不正确"),
    YZWT_YZWT2_FAILE(51114, "问题2验证失败：不存在此问题"),
    YZWT_YZWT2_DAFAILE(51115, "问题2验证失败：问题答案不正确"),
    YZWT_YZWT3_FAILE(51116, "问题3验证失败：不存在此问题"),
    YZWT_YZWT3_DAFAILE(51117, "问题3验证失败：问题答案不正确"),
    YZWT_WT_DAFAILE(51118, "获取问题：未找到相关验证问题，你可能不是个体纳税人或者企业纳税人"),

    SM_CHECKNSRSBH_FAILE(52001,"请输入正确的纳税人识别号!"),
    SM_CHECKPHONE_FAILE(52002,"输入法人电话有误，如果有变更请及时变更工商税务信息!"),
    YD_UPDATE_FAILE(52003,"未找到相关数据!"),
    SM_RZ_FAILE(52004,"实名认证出错，未查到相关信息!"),

    /* 接口错误：60001-69999 */
    INTERFACE_INNER_INVOKE_ERROR(60001, "内部系统接口调用异常"),
    INTERFACE_OUTTER_INVOKE_ERROR(60002, "外部系统接口调用异常"),
    INTERFACE_FORBID_VISIT(60003, "该接口禁止访问"),
    INTERFACE_ADDRESS_INVALID(60004, "接口地址无效"),
    INTERFACE_REQUEST_TIMEOUT(60005, "接口请求超时"),
    INTERFACE_EXCEED_LOAD(60006, "接口负载过高"),
    INTERFACE_THIRDPART_USER_VALIDATE_FAILE(60007, "第三方用户验证接口验证失败"),

    /* 权限错误：70001-79999 */
    PERMISSION_NO_ACCESS(70001, "无访问权限"),
    SYSTEM_MYSQL_ERROR(70002,"财税卫士-存储过程运行失败"),

    /* 数据同步错误：80001-89999 */
    DATASYNC_CONFIG_NOT_FOUND(80001, "数据同步配置未找到"),
    DATASYNC_CONFIG_TABLENAME_EMPTY(80002, "数据同步配置表名为空"),
    DATASYNC_JOB_IS_RUNNING(80003, "数据同步任务正在执行中"),
    DATASYNC_INTERNAL_ERROR(80004, "数据同步服务内部错误"),
    DATASYNC_CONFIG_NOT_VALID(80005, "数据同步任务配置无效"),
    DATASYNC_CRON_NOT_VALID(80006, "数据同步任务执行规则无效"),
    DATASYNC_TASK_NOT_FOUND(80007, "数据同步任务不存在"),
    DATASYNC_REMOTE_SERVER_ERROR(80008, "数据同步远程服务器内部错误"),

    /* 数据采集错误：90001-99999 */
    DATAACQU_UPLOAD_FILENAME_EMPTY(90001, "数据采集：上传文件名为空"),
    DATAACQU_UPLOAD_FILEDATA_EMPTY(90002, "数据采集：上传文件内容为空"),
    DATAACQU_UPLOAD_FILETYPE_UNSUPPORT(90003, "数据采集：不支持的文件类型"),
    DATAACQU_UPLOAD_FILEMAXSIZE(90004, "数据采集：文件大小超过限制"),
    DATAACQU_PREUPLOAD_FAILED(90005, "数据采集：预上传失败"),
    DATAACQU_FILE_VALID_FAILED(90006, "数据采集：效验失败"),
    DATAACQU_FILEDATABZK_NOTFOUND(90007, "选择上传文件版本和选择企业基础信息不符，请检查"),
    DATAACQU_FILE_NOTFOUND(90008, "选择上传文件版本和选择企业基础信息不符，请检查！"),
    DATAACQU_VALID_SSQ_CROSS(90009, "数据采集：所属期有交叉"),
    DATAACQU_BACKUP_FILE_FAILED(90010, "数据采集：备份文件失败"),
    DATAACQU_SAVE_FILE_FAILED(90011, "请检查基础信息及选择上传文件是否一致！"),
    DATAACQU_TEMP_FILENAME_NOT_CORRECT(90012, "选择上传文件版本和选择企业基础信息不符，请检查！"),
    DATAACQU_FILE_VALID_FAILED_SHEET_NOTFOUND(90013, "数据采集：效验失败, 表名未找到"),
    DATAACQU_FILE_VALID_FAILED_SHEET_TITLE_ERROR(90014, "数据采集：效验失败, 表第一行内容不匹配"),
    DATAACQU_FILE_VALID_FAILED_SHEET_COLUMNS_NOT_MATCH(90015, "数据采集：效验失败，表列数不匹配"),
    DATAACQU_FILE_VALID_FAILED_SHEET_ROWS_NOT_MATCH(90016, "数据采集：效验失败，表行数不匹配"),
    DATAACQU_FILE_VALID_FAILED_SHEET_CELL_NOT_DIGIT(90017, "数据采集：效验失败，表中单元格非数字"),
    DATAACQU_FILE_VALID_FAILED_SHEET_NSR_NOT_MATCH(90018, "数据采集：效验失败，纳税人信息不匹配"),
    DATAACQU_FILE_VALID_FAILED_SHEET_SSQQ_NOT_MATCH(90019, "数据采集：效验失败，所属期起始不匹配"),
    DATAACQU_FILE_VALID_FAILED_SHEET_SSQZ_NOT_MATCH(90020, "数据采集：效验失败，所属期截止不匹配"),
    DATAACQU_FILE_PARSE_FAILED(90021, "数据采集：解析失败"),
    DATAACQU_FILE_PARSE_FAILED_PATH_ERROR(90022, "数据采集：解析失败, 文件路径错误"),
    DATAACQU_FILE_PARSE_FAILED_SHEET_NOT_FOUND(90023, "数据采集：解析失败, 文件表格未找到"),
    DATAACQU_FILE_PARSE_FAILED_COMMON_ERROR(90024, "数据采集：解析失败, 文件表格映射公共类错误"),
    DATAACQU_FILE_PARSE_FAILED_DATA_ERROR(90025, "数据采集：解析失败, 文件表格映射数据类错误"),
    DATAACQU_FILE_SAVE_FAILED(90026, "数据采集：保存数据失败"),
    DATAACQU_FILE_PARSE_FAILED_ALREADY_SAVED(90027, "数据采集：解析失败, 该文件已被解析入库"),

    DATAACQU_FILE_PARSE_DATA_IS_INCOMPLETE(90030, "上传报表数据可能不完整，数据完整性影响指标运算的准确性，为真实反应企业财税风险，请检查填报数据，如已确认无误可点击确认！"),
    DATAACQU_FILE_PARSE_TYPE_IS_WRONG(90031, "可能原因为上传报表类型与用户所选企业类型不相符"),
    DATAACQU_FILE_PARSE_FAILURE(90032, "上传文件数据模板格式错误，请检查文件对应基础信息是否正确");
    private final int code;
    private String message;

    ResultCode(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public Integer code() {
        return this.code;
    }

    public String message() {
        return this.message;
    }

    public static String getMessage(String name) {
        for (ResultCode item : ResultCode.values()) {
            if (item.name().equals(name)) {
                return item.message;
            }
        }
        return name;
    }


    public static Integer getCode(String name) {
        for (ResultCode item : ResultCode.values()) {
            if (item.name().equals(name)) {
                return item.code;
            }
        }
        return null;
    }

}
