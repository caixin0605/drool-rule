package com.drool.engine.entity;

/**
 * @author 菜心
 * @date 2020/8/18
 * 数据项值
 */

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.math.BigDecimal;
import java.util.List;

@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class DataItem {
    /**
     * 指标编号
     */
    private String zbbh;

    /*资产负债表数据项定义*/
    /**
     * F_资产负债表_年初持有待售负债余款
     */
    private Double nccydsfzye_zcfz;
    /**
     * F_资产负债表_年初持有待售资产余款
     */
    private Double nccydszcye_zcfz;
    /**
     * F_资产负债表_年初持有至到期投资
     */
    private Double nccyzdqtz_zcfz;
    /**
     * F_资产负债表_年初存货
     */
    private Double ncch_zcfz;
    /**
     * F_资产负债表_年初递延收益余额
     */
    private Double ncdysyye_zcfz;
    /**
     * F_资产负债表_年初短期借款余额
     */
    private Double ncdqjkye_zcfz;
    /**
     * F_资产负债表_年初短期投资
     */
    private Double ncdqtz_zcfz;
    /**
     * F_资产负债表_年初非流动资产合计
     */
    private Double ncfldzchj_zcfz;
    /**
     * F_资产负债表_年初负债总额
     */
    private Double ncfzze_zcfz;
    /**
     * F_资产负债表_年初工程工资
     */
    private Double ncgcgz_zcfz;
    /**
     * F_资产负债表_年初固定资产净值
     */
    private Double ncgdzcjz_zcfz;
    /**
     * F_资产负债表_年初固定资产清理
     */
    private Double ncgdzcql_zcfz;
    /**
     * F_资产负债表_年初固定资产原值
     */
    private Double ncgdzcyz_zcfz;
    /**
     * F_资产负债表_年初累计折旧
     */
    private Double ncljzj_zcfz;
    /**
     * F_资产负债表_期末累计折旧
     */
    private Double qmljzj_zcfz;
    /**
     * F_资产负债表_年初货币资金
     */
    private Double nchbzj_zcfz;
    /**
     * F_资产负债表_年初开发支出
     */
    private Double nckfzc_zcfz;
    /**
     * F_资产负债表_年初可供出售金融资产
     */
    private Double nckgcsjrzc_zcfz;
    /**
     * F_资产负债表_年初流动资产合计
     */
    private Double ncldzchj_zcfz;
    /**
     * F_资产负债表_年初其他非流动负债
     */
    private Double ncqtfldfz_zcfz;
    /**
     * F_资产负债表_年初其他非流动资产
     */
    private Double ncqtfldzc_zcfz;
    /**
     * F_资产负债表_年初其他流动负债
     */
    private Double ncqtldfz_zcfz;
    /**
     * F_资产负债表_年初其他流动资产
     */
    private Double ncqtldzc_zcfz;
    /**
     * F_资产负债表_年初其他应付账款
     */
    private Double ncqtyfzk_zcfz;
    /**
     * F_资产负债表_年初其他应收款
     */
    private Double ncqtyszk_zcfz;
    /**
     * F_资产负债表_年初其它综合收益
     */
    private Double ncqtzhsy_zcfz;
    /**
     * F_资产负债表_年初商誉
     */
    private Double ncsy_zcfz;
    /**
     * F_资产负债表_年初生产性生物资产
     */
    private Double ncscxswzc_zcfz;
    /**
     * F_资产负债表_年初实收资本余额
     */
    private Double ncsszbye_zcfz;
    /**
     * F_资产负债表_年初所有者权益
     */
    private Double ncsyzqy_zcfz;
    /**
     * F_资产负债表_年初投资性房地产
     */
    private Double nctzxfdc_zcfz;
    /**
     * F_资产负债表_年初未分配利润
     */
    private Double ncwfplr_zcfz;
    /**
     * F_资产负债表_年初无形资产
     */
    private Double ncwxzc_zcfz;
    /**
     * F_资产负债表_年初一年内到期的非流动资产
     */
    private Double ncynndqdfldzc_zcfz;
    /**
     * F_资产负债表_年初以公允价值计量且其变动计入当期损益的金融资
     */
    private Double ncygyjzjlqqbdjrdqsydjrz_zcfz;
    /**
     * F_资产负债表_年初应付票据
     */
    private Double ncyfpj_zcfz;
    /**
     * F_资产负债表_年初应付账款
     */
    private Double ncyfzk_zcfz;
    /**
     * F_资产负债表_年初应收利息
     */
    private Double ncyslx_zcfz;
    /**
     * F_资产负债表_年初应收票据
     */
    private Double ncyspj_zcfz;
    /**
     * F_资产负债表_年初应收账款
     */
    private Double ncyszk_zcfz;
    /**
     * F_资产负债表_年初油气资产
     */
    private Double ncyqzc_zcfz;
    /**
     * F_资产负债表_年初预付账款
     */
    private Double ncyfuzk_zcfz;
    /**
     * F_资产负债表_年初预计负债余款
     */
    private Double ncyjfzye_zcfz;
    /**
     * F_资产负债表_年初预收账款
     */
    private Double ncyshuzk_zcfz;
    /**
     * F_资产负债表_年初在建工程
     */
    private Double nczjgc_zcfz;
    /**
     * F_资产负债表_年初长期待摊费用
     */
    private Double nccqdtfy_zcfz;
    /**
     * F_资产负债表_年初长期股权投资
     */
    private Double nccqgqtz_zcfz;
    /**
     * F_资产负债表_年初长期借款余款
     */
    private Double nccqjkye_zcfz;
    /**
     * F_资产负债表_年初长期应付款
     */
    private Double nccqyfk_zcfz;
    /**
     * F_资产负债表_年初长期债券投资
     */
    private Double nccqzjtz_zcfz;
    /**
     * F_资产负债表_年初专项应付款余款
     */
    private Double nczxyfkye_zcfz;
    /**
     * F_资产负债表_年初资本公积余额
     */
    private Double nczbgjye_zcfz;
    /**
     * F_资产负债表_年初资产总额
     */
    private Double nczcze_zcfz;
    /**
     * F_资产负债表_年初递延所得税财产
     */
    private Double ncdysdscc_zcfz;
    /**
     * F_资产负债表_期末持有待售负债余款
     */
    private Double qmcydsfzye_zcfz;
    /**
     * F_资产负债表_期末持有待售资产余款
     */
    private Double qmcydszcye_zcfz;
    /**
     * F_资产负债表_期末持有至到期投资
     */
    private Double qmcyzdqtz_zcfz;
    /**
     * F_资产负债表_期末存货
     */
    private Double qmch_zcfz;
    /**
     * F_资产负债表_期末存货_上期
     */
    private Double qmch_sq_zcfz;
    /**
     * F_资产负债表_期末递延收益余额
     */
    private Double qmdysyye_zcfz;
    /**
     * F_资产负债表_期末短期借款余额
     */
    private Double qmdqjkye_zcfz;
    /**
     * F_资产负债表_期末短期投资
     */
    private Double qmdqtz_zcfz;
    /**
     * F_资产负债表_期末非流动资产合计
     */
    private Double qmfldzchj_zcfz;
    /**
     * F_资产负债表_期末负债总额
     */
    private Double qmfzze_zcfz;
    /**
     * F_资产负债表_期末工程工资
     */
    private Double qmgcgz_zcfz;
    /**
     * F_资产负债表_期末固定资产合计
     */
    private Double qmgdzchj_zcfz;
    /**
     * F_资产负债表_期末固定资产净值
     */
    private Double qmgdzcjz_zcfz;
    /**
     * F_资产负债表_期末固定资产清理
     */
    private Double qmgdzcql_zcfz;
    /**
     * F_资产负债表_期末固定资产原值
     */
    private Double qmgdzcyz_zcfz;
    /**
     * F_资产负债表_期末固定资产原值_上期
     */
    private Double qmgdzcyz_sq_zcfz;
    /**
     * F_资产负债表_期末货币资金
     */
    private Double qmhbzj_zcfz;
    /**
     * F_资产负债表_期末开发支出
     */
    private Double qmkfzc_zcfz;
    /**
     * F_资产负债表_期末可供出售金融资产
     */
    private Double qmkgcsjrzc_zcfz;
    /**
     * F_资产负债表_期末流动负债合计
     */
    private Double qmldfzhj_zcfz;
    /**
     * F_资产负债表_期末流动资产合计
     */
    private Double qmldzchj_zcfz;
    /**
     * F_资产负债表_期末其他非流动负债
     */
    private Double qmqtfldfz_zcfz;
    /**
     * F_资产负债表_期末其他非流动资产
     */
    private Double qmqtfldzc_zcfz;
    /**
     * F_资产负债表_期末其他非流动资产_上期
     */
    private Double qmqtfldzc_sq_zcfz;
    /**
     * F_资产负债表_期末其他流动负债
     */
    private Double qmqtldfz_zcfz;
    /**
     * F_资产负债表_期末其他流动资产
     */
    private Double qmqtldzc_zcfz;
    /**
     * F_资产负债表_期末其他流动资产_上期
     */
    private Double qmqtldzc_sq_zcfz;
    /**
     * F_资产负债表_期末其他应付账款
     */
    private Double qmqtyfzk_zcfz;
    /**
     * F_资产负债表_期末其他应付账款_上期
     */
    private Double qmqtyfzk_sq_zcfz;
    /**
     * F_资产负债表_期末其他应收款
     */
    private Double qmqtysk_zcfz;
    /**
     * F_资产负债表_期末其他应收款_基期
     */
    private Double qmqtysk_jq_zcfz;
    /**
     * F_资产负债表_期末其他应收款_上期
     */
    private Double qmqtysk_sq_zcfz;
    /**
     * F_资产负债表_期末其它综合收益
     */
    private Double qmqtzhsy_zcfz;
    /**
     * F_资产负债表_期末商誉
     */
    private Double qmsy_zcfz;
    /**
     * F_资产负债表_期末生产性生物资产
     */
    private Double qmscxswzc_zcfz;
    /**
     * F_资产负债表_期末实收资本余额
     */
    private Double qmsszbye_zcfz;
    /**
     * F_资产负债表_期末所有者权益
     */
    private Double qmsyzqy_zcfz;
    /**
     * F_资产负债表_期末投资性房地产
     */
    private Double qmtzxfdc_zcfz;
    /**
     * F_资产负债表_期末未分配利润
     */
    private Double qmwfplr_zcfz;
    /**
     * F_资产负债表_期末无形资产
     */
    private Double qmwxzc_zcfz;
    /**
     * F_资产负债表_期末一年内到期的非流动资产
     */
    private Double qmynndqdfldzc_zcfz;
    /**
     * F_资产负债表_期末以公允价值计量且其变动计入当期损益的金融资
     */
    private Double qmygyjzjlqbdjrdqsydjrz_zcfz;
    /**
     * F_资产负债表_期末应付票据
     */
    private Double qmyfpj_zcfz;
    /**
     * F_资产负债表_期末应付债券
     */
    private Double qmyfzj_zcfz;
    /**
     * F_资产负债表_期末应付账款
     */
    private Double qmyfzk_zcfz;
    /**
     * F_资产负债表_期末应付账款_上期
     */
    private Double qmyfzk_sq_zcfz;
    /**
     * F_资产负债表_期末应收利息
     */
    private Double qmyslx_zcfz;
    /**
     * F_资产负债表_期末应收票据
     */
    private Double qmyspj_zcfz;
    /**
     * F_资产负债表_期末应收账款
     */
    private Double qmyszk_zcfz;
    /**
     * F_资产负债表_期末应收账款_上期
     */
    private Double qmyszk_sq_zcfz;
    /**
     * F_资产负债表_期末油气资产
     */
    private Double qmyqzc_zcfz;
    /**
     * F_资产负债表_期末预付账款
     */
    private Double qmyfuzk_zcfz;
    /**
     * F_资产负债表_期末预计负债余款
     */
    private Double qmyjfzye_zcfz;
    /**
     * F_资产负债表_期末预收账款
     */
    private Double qmyshuzk_zcfz;
    /**
     * F_资产负债表_期末在建工程
     */
    private Double qmzjgc_zcfz;
    /**
     * F_资产负债表_期末长期待摊费用
     */
    private Double qmcqdtfy_zcfz;
    /**
     * F_资产负债表_期末长期股权投资
     */
    private Double qmcqgqtz_zcfz;
    /**
     * F_资产负债表_期末长期借款余款
     */
    private Double qmcqjkye_zcfz;
    /**
     * F_资产负债表_期末长期应付款
     */
    private Double qmcqyfk_zcfz;
    /**
     * F_资产负债表_期末长期债券投资
     */
    private Double qmcqzjtz_zcfz;
    /**
     * F_资产负债表_期末专项应付款余款
     */
    private Double qmzxyfkye_zcfz;
    /**
     * F_资产负债表_期末资本公积余额
     */
    private Double qmzbgjye_zcfz;
    /**
     * F_资产负债表_期末资产总额
     */
    private Double qmzcze_zcfz;
    /**
     * F_资产负债表_期末递延所得税财产
     */
    private Double qmdysdscc_zcfz;


    /* 利润表数据项定义*/
    /**
     * F_利润表_财务费用_本年
     */
    private Double cwfyBn_lrb;
    /**
     * F_利润表_财务费用_本年_基期
     */
    private Double cwfyBn_jq_lrb;
    /**
     * F_利润表_公允价值变动收益损失_本年
     */
    private Double gyjzbdsyssBn_lrb;
    /**
     * F_利润表_管理费用_本年
     */
    private Double glfyBn_lrb;
    /**
     * F_利润表_管理费用_本年_基期
     */
    private Double glfyBn_jq_lrb;
    /**
     * F_利润表_净利润_本年
     */
    private Double jlrBn_lrb;
    /**
     * F_利润表_净利润_本年_基期
     */
    private Double jlrBn_jq_lrb;
    /**
     * F_利润表_利润总额_本年
     */
    private Double lrzeBn_lrb;
    /**
     * F_利润表_利润总额_本年_基期
     */
    private Double lrzeBn_jq_lrb;
    /**
     * F_利润表_其他收益_本年
     */
    private Double qtsyBn_lrb;
    /**
     * F_利润表_税金及附加_本年
     */
    private Double sjjfjBn_lrb;
    /**
     * F_利润表_投资收益_本年
     */
    private Double tzsyBn_lrb;
    /**
     * F_利润表_销售费用_本年
     */
    private Double xsfyBn_lrb;
    /**
     * F_利润表_销售费用_本年_基期
     */
    private Double xsfyBn_jq_lrb;
    /**
     * F_利润表_营业成本_本年
     */
    private Double yycbBn_lrb;
    /**
     * F_利润表_营业成本_本年_上期
     */
    private Double yycbBn_sq_lrb;
    /**
     * F_利润表_营业成本_本年_基期
     */
    private Double yycbBn_jq_lrb;
    /**
     * F_利润表_营业利润_本年
     */
    private Double yylrBn_lrb;
    /**
     * F_利润表_营业利润_本年_基期
     */
    private Double yylrBn_jq_lrb;
    /**
     * F_利润表_营业收入_本年
     */
    private Double yysrBn_lrb;
    /**
     * F_利润表_营业收入_本年_上期
     */
    private Double yysrBn_sq_lrb;
    /**
     * F_利润表_营业收入_本年_基期
     */
    private Double yysrBn_jq_lrb;
    /**
     * F_利润表_营业外收入_本年
     */
    private Double yywsrBn_lrb;
    /**
     * F_利润表_营业外支出_本年
     */
    private Double yywzcBn_lrb;
    /**
     * F_利润表_资产处置收益_本年
     */
    private Double zcczsyBn_lrb;
    /**
     * F_利润表_资产减值损失_本年
     */
    private Double zcjzssBn_lrb;
    /**
     * F_现金流量表_处置固定资产、无形资产和其他长期资产、非流动资产收回的现金净额_本年
     */
    private Double czgdzcwxzchqtcqzcfldzcshdxjjeBn_xjllb;
    /**
     * F_现金流量表_经营活动产生的现金流净额_本年
     */
    private Double jyhdcsdxjljeBn_xjllb;
    /**
     * F_现金流量表_经营活动现金流量净额_本年
     */
    private Double jyhdxjlljeBn_xjllb;
    /**
     * F_现金流量表_收到的其他与经营活动有关的资金_本年
     */
    private Double sddqtyjyhdygdzjBn_xjllb;
    /**
     * F_现金流量表_销售商品提供劳务收到的现金_本年
     */
    private Double xssptglwsddxjBn_xjllb;

    /*    所得税*/

    /**
     * F_所得税_减免所得税额_年累
     */
    private Double jmsdseNl_sds;
    /**
     * F_所得税_利润总额_本期
     */
    private Double lrzeBq_sds;
    /**
     * F_所得税_利润总额_年累
     */
    private Double lrzeNl_sds;
    /**
     * F_所得税_营业成本_本期
     */
    private Double yycbBq_sds;
    /**
     * F_所得税_营业成本_年累
     */
    private Double yycbNl_sds;
    /**
     * F_所得税_营业收入_本期
     */
    private Double yysrBq_sds;
    /**
     * F_所得税_营业收入_年累
     */
    private Double yysrNl_sds;
    /**
     * F_所得税_应纳所得税额_本期
     */
    private Double ynsdseBq_sds;
    /**
     * F_所得税_应纳所得税额_年累
     */
    private Double ynsdseNl_sds;

    /*    申报表*/
    /**
     * F_小规模纳税人_应纳税额减征额_本年
     */
    private Double ynsejzeBn_xgm;
    /**
     * F_小规模纳税人_应征增值税不含税销售额3_本年
     */
    private Double yzzzsbhxse3Bn_xgm;
    /**
     * F_小规模纳税人_税控器具开具的普通发票不含税销售额_本年
     */
    private Double skqjkjdptfpbhsxse_xgm;
    /**
     * F_小规模纳税人_税务机关代开的增值税专用发票不含税销售额_本年
     */
    private Double swjgdkdzzszyfpbhsxse_xgm;
    /**
     * F_小规模纳税人_销售使用过的固定资产不含税销售额_减免明细表
     */
    private Double xssygdgdzcbhsxsejmmxb_xgm;
    /**
     * F_小规模纳税人_减税项目_减免明细表
     */
    private Double jsxmjmmxb_xgm;
    /**
     * F_小规模纳税人_应纳销售额_本年
     */
    private Double ynxseBn_xgm;
    /**
     * F_小规模纳税人_应纳销售额_本年_基期
     */
    private Double ynxseBn_jq_xgm;
    /**
     * F_小规模纳税人_应纳销售额_本期
     */
    private Double ynxseBq_xgm;
    /**
     * F_小规模纳税人_应纳销售额_本期_上期
     */
    private Double ynxseBq_sq_xgm;
    /**
     * F_小规模纳税人_应纳税额合计_本年
     */
    private Double ynsehjBn_xgm;
    /**
     * F_小规模纳税人_应纳税额合计_本期
     */
    private Double ynsehjBq_xgm;
    /**
     * F_小规模纳税人_免税销售额_本期
     */
    private Double msxseBq_xgm;
    /**
     * F_小规模纳税人_免税销售额_本年
     */
    private Double msxseBn_xgm;
    /**
     * F_小规模纳税人_免抵退销售收入_本年
     */
    private Double mdtxssr_xgm;
    /**
     * F_小规模纳税人_应纳增值税_本年
     */
    private Double ynzzsBn_xgm;
    /**
     * F_一般纳税人_应纳税额减征额_本年
     */
    private Double ynsejzeBn_ybnsr;
    /**
     * F_一般纳税人_应纳税额合计_本年
     */
    private Double ynsehjBn_ybnsr;
    /**
     * F_一般纳税人_应纳税额合计_本期
     */
    private Double ynsehjBq_ybnsr;
    /**
     * F_一般纳税人_免税销售额_本期
     */
    private Double msxseBq_ybnsr;
    /**
     * F_一般纳税人_期末留抵税额_本年
     */
    private Double qmldseBn_ybnsr;
    /**
     * F_一般纳税人_期末留抵税额_本年_上期
     */
    private Double qmldseBn_sq_ybnsr;
    /**
     * F_一般纳税人_免税销售额_本年
     */
    private Double msxseBn_ybnsr;
    /**
     * F_一般纳税人_免抵退销售收入_本年
     */
    private Double mdtxxsr_ybnsr;
    /**
     * F_一般纳税人_不开票_销项应纳税额
     */
    private Double bkpxsynse_ybnsr;
    /**
     * F_一般纳税人_不开票收入
     */
    private Double bkpsr_ybnsr;
    /**
     * F_一般纳税人_当期申报抵扣进项税额合计_附表2
     */
    private Double dqsbdkjxsehjfb2_ybnsr;
    /**
     * F_一般纳税人_税控系统专用设备费及技术维护费_附表4
     */
    private Double xkxtzysbfjjswhffb4_ybnsr;
    /**
     * F_一般纳税人_减税项目_减免明细表
     */
    private Double jsxmjmmxb_ybnsr;
    /**
     * F_一般纳税人_既征增值税销售额_本年
     */
    private Double jzzzsxseBn_ybnsr;
    /**
     * F_一般纳税人_开具其他发票_销项应纳税额
     */
    private Double kjqtfpxsynse_ybnsr;
    /**
     * F_一般纳税人_开具其他发票收入
     */
    private Double kjqtfpsr_ybnsr;
    /**
     * F_一般纳税人_简易办法纳税检查调整_本年
     */
    private Double jybfnsjctzBn_ybnsr;
    /**
     * F_一般纳税人_销项税额_本年
     */
    private Double xxseBn_ybnsr;
    /**
     * F_一般纳税人_销项税额_本年_上期
     */
    private Double xxseBn_sq_ybnsr;
    /**
     * F_一般纳税人_进项税额_本年
     */
    private Double jxseBn_ybnsr;
    /**
     * F_一般纳税人_进项税额_本年_上期
     */
    private Double jxseBn_sq_ybnsr;
    /**
     * F_一般纳税人_农产品收购发票或者销售发票税额_附表2
     */
    private Double ncpsgfphzxsfpsefb2_ybnsr;
    /**
     * F_一般纳税人_申报抵扣进项税额_本年
     */
    private Double sbdkjxseBn_ybnsr;
    /**
     * F_一般纳税人_申报抵扣进项税额转出_本年
     */
    private Double sbdkjxsezcBn_ybnsr;
    /**
     * F_一般纳税人_销售额合计_附表1
     */
    private Double xsehjfb1_ybnsr;
    /**
     * F_一般纳税人_本期进项税额转出额_附表2
     */
    private Double bqjxsezcefb2_ybnsr;
    /**
     * F_一般纳税人_非正常损失_附表2
     */
    private Double fzcssfb2_ybnsr;
    /**
     * F_一般纳税人_应纳销售额_本年
     */
    private Double ynxseBn_ybnsr;
    /**
     * F_一般纳税人_应纳销售额_本年_基期
     */
    private Double ynxseBn_jq_ybnsr;
    /**
     * F_一般纳税人_应纳销售额_本期
     */
    private Double ynxseBq_ybnsr;
    /**
     * F_一般纳税人_应纳销售额_本期_上期
     */
    private Double ynxseBq_sq_ybnsr;
    /**
     * F_一般纳税人_应纳增值税额_本年
     */
    private Double ynzzseBn_ybnsr;
    /**
     * F_一般纳税人_应纳增值税额_本年_上期
     */
    private Double ynzzseBn_sq_ybnsr;
    /**
     * F_一般纳税人_应纳增值税额_本年_基期
     */
    private Double ynzzseBn_jq_ybnsr;
    /**
     * F_一般纳税人_专用发票_销项应纳税额
     */
    private Double zyfpxxynse_ybnsr;
    /**
     * F_一般纳税人_专用发票收入
     */
    private Double zyfpsr_ybnsr;
    /**
     * F_一般纳税人_6税率销售额_附表1
     */
    private Double sl6xse_b1_ybnsr;
    /**
     * F_一般纳税人_6税率开具增值税专用发票_附表1
     */
    private Double sl6kjzzszyfp_b1_ybnsr;
    /**
     * F_一般纳税人_6税率开具其他发票_附表1
     */
    private Double sl6kjqtfp_b1_ybnsr;
    /**
     * F_一般纳税人_16税率销售额_附表1
     */
    private Double sl16xse_b1_ybnsr;
    /**
     * 明细结果集
     */
    private List<HZjbgMx> list;

    /**
     * 数据项赋值 一个存储函数对应一个值
     *
     * @param hsm  存储函数名
     * @param data 数据项值
     */
    public void setFile(String hsm, Double data) {
        if (hsm == null) {
            return;
        }
        if (hsm.startsWith("F_资产负债表")) {
            switch (hsm) {
                case "F_资产负债表_年初持有待售负债余款":
                    this.nccydsfzye_zcfz = data;
                    break;
                case "F_资产负债表_年初持有待售资产余款":
                    this.nccydszcye_zcfz = data;
                    break;
                case "F_资产负债表_年初持有至到期投资":
                    this.nccyzdqtz_zcfz = data;
                    break;
                case "F_资产负债表_年初存货":
                    this.ncch_zcfz = data;
                    break;
                case "F_资产负债表_年初递延收益余额":
                    this.ncdysyye_zcfz = data;
                    break;
                case "F_资产负债表_年初短期借款余额":
                    this.ncdqjkye_zcfz = data;
                    break;
                case "F_资产负债表_年初短期投资":
                    this.ncdqtz_zcfz = data;
                    break;
                case "F_资产负债表_年初非流动资产合计":
                    this.ncfldzchj_zcfz = data;
                    break;
                case "F_资产负债表_年初负债总额":
                    this.ncfzze_zcfz = data;
                    break;
                case "F_资产负债表_年初工程工资":
                    this.ncgcgz_zcfz = data;
                    break;
                case "F_资产负债表_年初固定资产净值":
                    this.ncgdzcjz_zcfz = data;
                    break;
                case "F_资产负债表_年初固定资产清理":
                    this.ncgdzcql_zcfz = data;
                    break;
                case "F_资产负债表_年初固定资产原值":
                    this.ncgdzcyz_zcfz = data;
                    break;
                case "F_资产负债表_年初货币资金":
                    this.nchbzj_zcfz = data;
                    break;
                case "F_资产负债表_年初开发支出":
                    this.nckfzc_zcfz = data;
                    break;
                case "F_资产负债表_年初可供出售金融资产":
                    this.nckgcsjrzc_zcfz = data;
                    break;
                case "F_资产负债表_年初流动资产合计":
                    this.ncldzchj_zcfz = data;
                    break;
                case "F_资产负债表_年初其他非流动负债":
                    this.ncqtfldfz_zcfz = data;
                    break;
                case "F_资产负债表_年初其他非流动资产":
                    this.ncqtfldzc_zcfz = data;
                    break;
                case "F_资产负债表_年初其他流动负债":
                    this.ncqtldfz_zcfz = data;
                    break;
                case "F_资产负债表_年初其他流动资产":
                    this.ncqtldzc_zcfz = data;
                    break;
                case "F_资产负债表_年初其他应付账款":
                    this.ncqtyfzk_zcfz = data;
                    break;
                case "F_资产负债表_年初其他应收款":
                    this.ncqtyszk_zcfz = data;
                    break;
                case "F_资产负债表_年初其它综合收益":
                    this.ncqtzhsy_zcfz = data;
                    break;
                case "F_资产负债表_年初商誉":
                    this.ncsy_zcfz = data;
                    break;
                case "F_资产负债表_年初生产性生物资产":
                    this.ncscxswzc_zcfz = data;
                    break;
                case "F_资产负债表_年初实收资本余额":
                    this.ncsszbye_zcfz = data;
                    break;
                case "F_资产负债表_年初所有者权益":
                    this.ncsyzqy_zcfz = data;
                    break;
                case "F_资产负债表_年初投资性房地产":
                    this.nctzxfdc_zcfz = data;
                    break;
                case "F_资产负债表_年初未分配利润":
                    this.ncwfplr_zcfz = data;
                    break;
                case "F_资产负债表_年初无形资产":
                    this.ncwxzc_zcfz = data;
                    break;
                case "F_资产负债表_年初一年内到期的非流动资产":
                    this.ncynndqdfldzc_zcfz = data;
                    break;
                case "F_资产负债表_年初以公允价值计量且其变动计入当期损益的金融资":
                    this.ncygyjzjlqqbdjrdqsydjrz_zcfz = data;
                    break;
                case "F_资产负债表_年初应付票据":
                    this.ncyfpj_zcfz = data;
                    break;
                case "F_资产负债表_年初应付账款":
                    this.ncyfzk_zcfz = data;
                    break;
                case "F_资产负债表_年初应收利息":
                    this.ncyslx_zcfz = data;
                    break;
                case "F_资产负债表_年初应收票据":
                    this.ncyspj_zcfz = data;
                    break;
                case "F_资产负债表_年初应收账款":
                    this.ncyszk_zcfz = data;
                    break;
                case "F_资产负债表_年初油气资产":
                    this.ncyqzc_zcfz = data;
                    break;
                case "F_资产负债表_年初预付账款":
                    this.ncyfuzk_zcfz = data;
                    break;
                case "F_资产负债表_年初预计负债余款":
                    this.ncyjfzye_zcfz = data;
                    break;
                case "F_资产负债表_年初预收账款":
                    this.ncyshuzk_zcfz = data;
                    break;
                case "F_资产负债表_年初在建工程":
                    this.nczjgc_zcfz = data;
                    break;
                case "F_资产负债表_年初长期待摊费用":
                    this.nccqdtfy_zcfz = data;
                    break;
                case "F_资产负债表_年初长期股权投资":
                    this.nccqgqtz_zcfz = data;
                    break;
                case "F_资产负债表_年初长期借款余款":
                    this.nccqjkye_zcfz = data;
                    break;
                case "F_资产负债表_年初长期应付款":
                    this.nccqyfk_zcfz = data;
                    break;
                case "F_资产负债表_年初长期债券投资":
                    this.nccqzjtz_zcfz = data;
                    break;
                case "F_资产负债表_年初专项应付款余款":
                    this.nczxyfkye_zcfz = data;
                    break;
                case "F_资产负债表_年初资本公积余额":
                    this.nczbgjye_zcfz = data;
                    break;
                case "F_资产负债表_年初资产总额":
                    this.nczcze_zcfz = data;
                    break;
                case "F_资产负债表_期末持有待售负债余款":
                    this.qmcydsfzye_zcfz = data;
                    break;
                case "F_资产负债表_期末持有待售资产余款":
                    this.qmcydszcye_zcfz = data;
                    break;
                case "F_资产负债表_期末持有至到期投资":
                    this.qmcyzdqtz_zcfz = data;
                    break;
                case "F_资产负债表_期末存货":
                    this.qmch_zcfz = data;
                    break;
                case "F_资产负债表_期末存货_上期":
                    this.qmch_sq_zcfz = data;
                    break;
                case "F_资产负债表_期末递延收益余额":
                    this.qmdysyye_zcfz = data;
                    break;
                case "F_资产负债表_期末短期借款余额":
                    this.qmdqjkye_zcfz = data;
                    break;
                case "F_资产负债表_期末短期投资":
                    this.qmdqtz_zcfz = data;
                    break;
                case "F_资产负债表_期末非流动资产合计":
                    this.qmfldzchj_zcfz = data;
                    break;
                case "F_资产负债表_期末负债总额":
                    this.qmfzze_zcfz = data;
                    break;
                case "F_资产负债表_期末工程工资":
                    this.qmgcgz_zcfz = data;
                    break;
                case "F_资产负债表_期末固定资产合计":
                    this.qmgdzchj_zcfz = data;
                    break;
                case "F_资产负债表_期末固定资产净值":
                    this.qmgdzcjz_zcfz = data;
                    break;
                case "F_资产负债表_期末固定资产清理":
                    this.qmgdzcql_zcfz = data;
                    break;
                case "F_资产负债表_期末固定资产原值":
                    this.qmgdzcyz_zcfz = data;
                    break;
                case "F_资产负债表_期末固定资产原值_上期":
                    this.qmgdzcyz_sq_zcfz = data;
                    break;
                case "F_资产负债表_期末货币资金":
                    this.qmhbzj_zcfz = data;
                    break;
                case "F_资产负债表_期末开发支出":
                    this.qmkfzc_zcfz = data;
                    break;
                case "F_资产负债表_期末可供出售金融资产":
                    this.qmkgcsjrzc_zcfz = data;
                    break;
                case "F_资产负债表_期末流动负债合计":
                    this.qmldfzhj_zcfz = data;
                    break;
                case "F_资产负债表_期末流动资产合计":
                    this.qmldzchj_zcfz = data;
                    break;
                case "F_资产负债表_期末其他非流动负债":
                    this.qmqtfldfz_zcfz = data;
                    break;
                case "F_资产负债表_期末其他非流动资产":
                    this.qmqtfldzc_zcfz = data;
                    break;
                case "F_资产负债表_期末其他非流动资产_上期":
                    this.qmqtfldzc_sq_zcfz = data;
                    break;
                case "F_资产负债表_期末其他流动负债":
                    this.qmqtldfz_zcfz = data;
                    break;
                case "F_资产负债表_期末其他流动资产":
                    this.qmqtldzc_zcfz = data;
                    break;
                case "F_资产负债表_期末其他流动资产_上期":
                    this.qmqtldzc_sq_zcfz = data;
                    break;
                case "F_资产负债表_期末其他应付账款":
                    this.qmqtyfzk_zcfz = data;
                    break;
                case "F_资产负债表_期末其他应付账款_上期":
                    this.qmqtyfzk_sq_zcfz = data;
                    break;
                case "F_资产负债表_期末其他应收款":
                    this.qmqtysk_zcfz = data;
                    break;
                case "F_资产负债表_期末其他应收款_基期":
                    this.qmqtysk_jq_zcfz = data;
                    break;
                case "F_资产负债表_期末其他应收款_上期":
                    this.qmqtysk_sq_zcfz = data;
                    break;
                case "F_资产负债表_期末其它综合收益":
                    this.qmqtzhsy_zcfz = data;
                    break;
                case "F_资产负债表_期末商誉":
                    this.qmsy_zcfz = data;
                    break;
                case "F_资产负债表_期末生产性生物资产":
                    this.qmscxswzc_zcfz = data;
                    break;
                case "F_资产负债表_期末实收资本余额":
                    this.qmsszbye_zcfz = data;
                    break;
                case "F_资产负债表_期末所有者权益":
                    this.qmsyzqy_zcfz = data;
                    break;
                case "F_资产负债表_期末投资性房地产":
                    this.qmtzxfdc_zcfz = data;
                    break;
                case "F_资产负债表_期末未分配利润":
                    this.qmwfplr_zcfz = data;
                    break;
                case "F_资产负债表_期末无形资产":
                    this.qmwxzc_zcfz = data;
                    break;
                case "F_资产负债表_期末一年内到期的非流动资产":
                    this.qmynndqdfldzc_zcfz = data;
                    break;
                case "F_资产负债表_期末以公允价值计量且其变动计入当期损益的金融资":
                    this.qmygyjzjlqbdjrdqsydjrz_zcfz = data;
                    break;
                case "F_资产负债表_期末应付票据":
                    this.qmyfpj_zcfz = data;
                    break;
                case "F_资产负债表_期末应付债券":
                    this.qmyfzj_zcfz = data;
                    break;
                case "F_资产负债表_期末应付账款":
                    this.qmyfzk_zcfz = data;
                    break;
                case "F_资产负债表_期末应付账款_上期":
                    this.qmyfzk_sq_zcfz = data;
                    break;
                case "F_资产负债表_期末应收利息":
                    this.qmyslx_zcfz = data;
                    break;
                case "F_资产负债表_期末应收票据":
                    this.qmyspj_zcfz = data;
                    break;
                case "F_资产负债表_期末应收账款":
                    this.qmyszk_zcfz = data;
                    break;
                case "F_资产负债表_期末应收账款_上期":
                    this.qmyszk_sq_zcfz = data;
                    break;
                case "F_资产负债表_期末油气资产":
                    this.qmyqzc_zcfz = data;
                    break;
                case "F_资产负债表_期末预付账款":
                    this.qmyfuzk_zcfz = data;
                    break;
                case "F_资产负债表_期末预计负债余款":
                    this.qmyjfzye_zcfz = data;
                    break;
                case "F_资产负债表_期末预收账款":
                    this.qmyshuzk_zcfz = data;
                    break;
                case "F_资产负债表_期末在建工程":
                    this.qmzjgc_zcfz = data;
                    break;
                case "F_资产负债表_期末长期待摊费用":
                    this.qmcqdtfy_zcfz = data;
                    break;
                case "F_资产负债表_期末长期股权投资":
                    this.qmcqgqtz_zcfz = data;
                    break;
                case "F_资产负债表_期末长期借款余款":
                    this.qmcqjkye_zcfz = data;
                    break;
                case "F_资产负债表_期末长期应付款":
                    this.qmcqyfk_zcfz = data;
                    break;
                case "F_资产负债表_期末长期债券投资":
                    this.qmcqzjtz_zcfz = data;
                    break;
                case "F_资产负债表_期末专项应付款余款":
                    this.qmzxyfkye_zcfz = data;
                    break;
                case "F_资产负债表_期末资本公积余额":
                    this.qmzbgjye_zcfz = data;
                    break;
                case "F_资产负债表_期末资产总额":
                    this.qmzcze_zcfz = data;
                    break;
                case "F_资产负债表_年初递延所得税财产":
                    this.ncdysdscc_zcfz = data;
                    break;
                case "F_资产负债表_期末递延所得税财产":
                    this.qmdysdscc_zcfz = data;
                    break;
            }
        }
        if (hsm.startsWith("F_利润表")) {
            switch (hsm) {
                case "F_利润表_财务费用_本年":
                    this.cwfyBn_lrb = data;
                    break;
                case "F_利润表_财务费用_本年_基期":
                    this.cwfyBn_jq_lrb = data;
                    break;
                case "F_利润表_公允价值变动收益损失_本年":
                    this.gyjzbdsyssBn_lrb = data;
                    break;
                case "F_利润表_管理费用_本年":
                    this.glfyBn_lrb = data;
                    break;
                case "F_利润表_管理费用_本年_基期":
                    this.glfyBn_jq_lrb = data;
                    break;
                case "F_利润表_净利润_本年":
                    this.jlrBn_lrb = data;
                    break;
                case "F_利润表_净利润_本年_基期":
                    this.jlrBn_jq_lrb = data;
                    break;
                case "F_利润表_利润总额_本年":
                    this.lrzeBn_lrb = data;
                    break;
                case "F_利润表_利润总额_本年_基期":
                    this.lrzeBn_jq_lrb = data;
                    break;
                case "F_利润表_其他收益_本年":
                    this.qtsyBn_lrb = data;
                    break;
                case "F_利润表_税金及附加_本年":
                    this.sjjfjBn_lrb = data;
                    break;
                case "F_利润表_投资收益_本年":
                    this.tzsyBn_lrb = data;
                    break;
                case "F_利润表_销售费用_本年":
                    this.xsfyBn_lrb = data;
                    break;
                case "F_利润表_销售费用_本年_基期":
                    this.xsfyBn_jq_lrb = data;
                    break;
                case "F_利润表_营业成本_本年":
                    this.yycbBn_lrb = data;
                    break;
                case "F_利润表_营业成本_本年_上期":
                    this.yycbBn_sq_lrb = data;
                    break;
                case "F_利润表_营业成本_本年_基期":
                    this.yycbBn_jq_lrb = data;
                    break;
                case "F_利润表_营业利润_本年":
                    this.yylrBn_lrb = data;
                    break;
                case "F_利润表_营业利润_本年_基期":
                    this.yylrBn_jq_lrb = data;
                    break;
                case "F_利润表_营业收入_本年":
                    this.yysrBn_lrb = data;
                    break;
                case "F_利润表_营业收入_本年_上期":
                    this.yysrBn_sq_lrb = data;
                    break;
                case "F_利润表_营业收入_本年_基期":
                    this.yysrBn_jq_lrb = data;
                    break;
                case "F_利润表_营业外收入_本年":
                    this.yywsrBn_lrb = data;
                    break;
                case "F_利润表_营业外支出_本年":
                    this.yywzcBn_lrb = data;
                    break;
                case "F_利润表_资产处置收益_本年":
                    this.zcczsyBn_lrb = data;
                    break;
                case "F_利润表_资产减值损失_本年":
                    this.zcjzssBn_lrb = data;
                    break;
            }
        }
        if (hsm.startsWith("F_现金流量表")) {
            switch (hsm) {
                case "F_现金流量表_处置固定资产、无形资产和其他长期资产、非流动资产收回的现金净额_本年":
                    this.czgdzcwxzchqtcqzcfldzcshdxjjeBn_xjllb = data;
                    break;
                case "F_现金流量表_经营活动产生的现金流净额_本年":
                    this.jyhdcsdxjljeBn_xjllb = data;
                    break;
                case "F_现金流量表_经营活动现金流量净额_本年":
                    this.jyhdxjlljeBn_xjllb = data;
                    break;
                case "F_现金流量表_收到的其他与经营活动有关的资金_本年":
                    this.sddqtyjyhdygdzjBn_xjllb = data;
                    break;
                case "F_现金流量表_销售商品提供劳务收到的现金_本年":
                    this.xssptglwsddxjBn_xjllb = data;
                    break;
            }
        }
        if (hsm.startsWith("F_所得税")) {
            switch (hsm) {
                case "F_所得税_减免所得税额_年累":
                    this.jmsdseNl_sds = data;
                    break;
                case "F_所得税_利润总额_本期":
                    this.lrzeBq_sds = data;
                    break;
                case "F_所得税_利润总额_年累":
                    this.lrzeNl_sds = data;
                    break;
                case "F_所得税_营业成本_本期":
                    this.yycbBq_sds = data;
                    break;
                case "F_所得税_营业成本_年累":
                    this.yycbNl_sds = data;
                    break;
                case "F_所得税_营业收入_本期":
                    this.yysrBq_sds = data;
                    break;
                case "F_所得税_营业收入_年累":
                    this.yysrNl_sds = data;
                    break;
                case "F_所得税_应纳所得税额_本期":
                    this.ynsdseBq_sds = data;
                    break;
                case "F_所得税_应纳所得税额_年累":
                    this.ynsdseNl_sds = data;
                    break;
            }
        }
        if (hsm.startsWith("F_一般纳税人")) {
            switch (hsm) {
                case "F_一般纳税人_减税项目_减免明细表":
                    this.jsxmjmmxb_ybnsr = data;
                    break;
                case "F_一般纳税人_税控系统专用设备费及技术维护费_附表4":
                    this.xkxtzysbfjjswhffb4_ybnsr = data;
                    break;
                case "F_一般纳税人_本期进项税额转出额_附表2":
                    this.bqjxsezcefb2_ybnsr = data;
                    break;
                case "F_一般纳税人_非正常损失_附表2":
                    this.fzcssfb2_ybnsr = data;
                    break;
                case "F_一般纳税人_6税率销售额_附表1":
                    this.sl6xse_b1_ybnsr = data;
                    break;
                case "F_一般纳税人_6税率开具其他发票_附表1":
                    this.sl6kjqtfp_b1_ybnsr = data;
                    break;
                case "F_一般纳税人_6税率开具增值税专用发票_附表1":
                    this.sl6kjzzszyfp_b1_ybnsr = data;
                    break;
                case "F_一般纳税人_16税率销售额_附表1":
                    this.sl16xse_b1_ybnsr = data;
                    break;
                case "F_一般纳税人_期末留抵税额_本年":
                    this.qmldseBn_ybnsr = data;
                    break;
                case "F_一般纳税人_销项税额_本年":
                    this.xxseBn_ybnsr = data;
                    break;
                case "F_一般纳税人_进项税额_本年":
                    this.jxseBn_ybnsr = data;
                    break;
                case "F_一般纳税人_销项税额_本年_上期":
                    this.xxseBn_sq_ybnsr = data;
                    break;
                case "F_一般纳税人_进项税额_本年_上期":
                    this.jxseBn_sq_ybnsr = data;
                    break;
                case "F_一般纳税人_简易办法纳税检查调整_本年":
                    this.jybfnsjctzBn_ybnsr = data;
                    break;
                case "F_一般纳税人_免抵退销售收入_本年":
                    this.mdtxxsr_ybnsr = data;
                    break;
                case "F_一般纳税人_应纳税额合计_本年":
                    this.ynsehjBn_ybnsr = data;
                    break;
                case "F_一般纳税人_应纳税额合计_本期":
                    this.ynsehjBq_ybnsr = data;
                    break;
                case "F_一般纳税人_应纳税额减征额_本年":
                    this.ynsejzeBn_ybnsr = data;
                    break;
                case "F_一般纳税人_免税销售额_本期":
                    this.msxseBq_ybnsr = data;
                    break;
                case "F_一般纳税人_免税销售额_本年":
                    this.msxseBn_ybnsr = data;
                    break;
                case "F_一般纳税人_不开票_销项应纳税额":
                    this.bkpxsynse_ybnsr = data;
                    break;
                case "F_一般纳税人_不开票收入":
                    this.bkpsr_ybnsr = data;
                    break;
                case "F_一般纳税人_当期申报抵扣进项税额合计_附表2":
                    this.dqsbdkjxsehjfb2_ybnsr = data;
                    break;
                case "F_一般纳税人_既征增值税销售额_本年":
                    this.jzzzsxseBn_ybnsr = data;
                    break;
                case "F_一般纳税人_开具其他发票_销项应纳税额":
                    this.kjqtfpxsynse_ybnsr = data;
                    break;
                case "F_一般纳税人_开具其他发票收入":
                    this.kjqtfpsr_ybnsr = data;
                    break;
                case "F_一般纳税人_农产品收购发票或者销售发票税额_附表2":
                    this.ncpsgfphzxsfpsefb2_ybnsr = data;
                    break;
                case "F_一般纳税人_申报抵扣进项税额_本年":
                    this.sbdkjxseBn_ybnsr = data;
                    break;
                case "F_一般纳税人_申报抵扣进项税额转出_本年":
                    this.sbdkjxsezcBn_ybnsr = data;
                    break;
                case "F_一般纳税人_销售额合计_附表1":
                    this.xsehjfb1_ybnsr = data;
                    break;
                case "F_一般纳税人_应纳销售额_本年":
                    this.ynxseBn_ybnsr = data;
                    break;
                case "F_一般纳税人_应纳销售额_本年_基期":
                    this.ynxseBn_jq_ybnsr = data;
                    break;
                case "F_一般纳税人_应纳销售额_本期":
                    this.ynxseBq_ybnsr = data;
                    break;
                case "F_一般纳税人_应纳销售额_本期_上期":
                    this.ynxseBq_sq_ybnsr = data;
                    break;
                case "F_一般纳税人_应纳增值税额_本年":
                    this.ynzzseBn_ybnsr = data;
                    break;
                case "F_一般纳税人_应纳增值税额_本年_上期":
                    this.ynzzseBn_sq_ybnsr = data;
                    break;
                case "F_一般纳税人_应纳增值税额_本年_基期":
                    this.ynzzseBn_jq_ybnsr = data;
                    break;
                case "F_一般纳税人_专用发票_销项应纳税额":
                    this.zyfpxxynse_ybnsr = data;
                    break;
                case "F_一般纳税人_专用发票收入":
                    this.zyfpsr_ybnsr = data;
                    break;
            }
        }
        if (hsm.startsWith("F_小规模纳税人")) {
            switch (hsm) {
                case "F_小规模纳税人_税务机关代开的增值税专用发票不含税销售额_本年":
                    this.swjgdkdzzszyfpbhsxse_xgm = data;
                    break;
                case "F_小规模纳税人_税控器具开具的普通发票不含税销售额_本年":
                    this.skqjkjdptfpbhsxse_xgm = data;
                    break;
                case "F_小规模纳税人_应征增值税不含税销售额3_本年":
                    this.yzzzsbhxse3Bn_xgm = data;
                    break;
                case "F_小规模纳税人_减税项目_减免明细表":
                    this.jsxmjmmxb_xgm = data;
                    break;
                case "F_小规模纳税人_销售使用过的固定资产不含税销售额_减免明细表":
                    this.xssygdgdzcbhsxsejmmxb_xgm = data;
                    break;
                case "F_小规模纳税人_免抵退销售收入_本年":
                    this.mdtxssr_xgm = data;
                    break;
                case "F_小规模纳税人_应纳税额合计_本年":
                    this.ynsehjBn_xgm = data;
                    break;
                case "F_小规模纳税人_应纳税额合计_本期":
                    this.ynsehjBq_xgm = data;
                    break;
                case "F_小规模纳税人_应纳税额减征额_本年":
                    this.ynsejzeBn_xgm = data;
                    break;
                case "F_小规模纳税人_应纳销售额_本年":
                    this.ynxseBn_xgm = data;
                    break;
                case "F_小规模纳税人_应纳销售额_本年_基期":
                    this.ynxseBn_jq_xgm = data;
                    break;
                case "F_小规模纳税人_应纳销售额_本期":
                    this.ynxseBq_xgm = data;
                    break;
                case "F_小规模纳税人_应纳销售额_本期_上期":
                    this.ynxseBq_sq_xgm = data;
                    break;
                case "F_小规模纳税人_应纳增值税_本年":
                    this.ynzzsBn_xgm = data;
                    break;
                case "F_小规模纳税人_免税销售额_本期":
                    this.msxseBq_xgm = data;
                    break;
            }
        }
    }
}
