package com.drool.engine.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.io.Serializable;
import java.util.Date;

/**
 * 【请填写功能名称】对象 h_zjbg_log
 *
 * @author xincai
 * @date 2020-08-18
 */
@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
@TableName(value = "h_zjbg_log")
public class HZjbgLog implements Serializable {
    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.UUID)
    private String id;

    @TableField(value = "bgbh")
    private String bgbh;

    @TableField(value = "start_time")
    private Date startTime;

    @TableField(value = "end_time")
    private Date endTime;

    @TableField(value = "duration")
    private Long duration;

    @TableField(value = "ret_code")
    private String retCode;

    @TableField(value = "ret_message")
    private String retMessage;

    @TableField(value = "create_date")
    private Date createDate;

    @TableField(value = "modifi_date")
    private Date modifiDate;

    @TableField(value = "bsblag")
    private String bsblag;

    @TableField(value = "bz")
    private String bz;

    @TableField(value = "h_zjbg_qyjbxx")
    private Long hZjbgQyjbxx;

    @TableField(value = "h_zjbg_qyszpm")
    private Long hZjbgQyszpm;

    @TableField(value = "h_zjbg")
    private Long hZjbg;

    @TableField(value = "h_zjbg_zbyxjgb")
    private Long hZjbgZbyxjgb;

    @TableField(value = "h_zjbg_mx")
    private Long hZjbgMx;

    @TableField(value = "zbbh")
    private String zbbh;

    @TableField(value = "zbmc")
    private String zbmc;

    @TableField(value = "djxh")
    private String djxh;

    /**
     * 目录编码
     */
    @TableField(value = "fl_bm")
    private String flBm;

    /**
     * 上级id
     */
    @TableField(value = "sj_id")
    private String sjId;

}
