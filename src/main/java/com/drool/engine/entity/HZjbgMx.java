package com.drool.engine.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * 指标运行结果明细对象 h_zjbg_mx
 *
 * @author fly
 * @date 2020-08-30
 */
@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
@TableName(value = "h_zjbg_mx")
public class HZjbgMx implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.UUID)
    private String id;

    /**
     * 指标结果明细ID
     */
    @TableField(value = "yxid")
    private String yxid;

    /**
     * $column.columnComment
     */
    @TableField(value = "djxh")
    private String djxh;

    /**
     * $column.columnComment
     */
    @TableField(value = "bgbh")
    private String bgbh;

    /**
     * 指标编号
     */
    @TableField(value = "zbbh")
    private String zbbh;

    /**
     * sx1
     */
    @TableField(value = "sx1")
    private String sx1;

    /**
     * 数据项2
     */
    @TableField(value = "sx2")
    private String sx2;

    /**
     * 数据项3
     */
    @TableField(value = "sx3")
    private String sx3;

    /**
     * 数据项4
     */
    @TableField(value = "sx4")
    private String sx4;

    /**
     * 数据项5
     */
    @TableField(value = "sx5")
    private String sx5;

    /**
     * 数据项6
     */
    @TableField(value = "sx6")
    private String sx6;

    /**
     * 数据项7
     */
    @TableField(value = "sx7")
    private String sx7;

    /**
     * 数据项8
     */
    @TableField(value = "sx8")
    private String sx8;

    /**
     * 数据项9
     */
    @TableField(value = "sx9")
    private String sx9;

    /**
     * 数据项10
     */
    @TableField(value = "sx10")
    private String sx10;

    /**
     * 业务时间起
     */
    @TableField(value = "ywsjq")
    private Date ywsjq;

    /**
     * 业务时间止
     */
    @TableField(value = "ywsjz")
    private Date ywsjz;

    /**
     * 数据描述
     */
    @TableField(value = "sjms")
    private String sjms;

    /**
     * 创建时间
     */
    @TableField(value = "create_date")
    private Date createDate;

    /**
     * 修改时间
     */
    @TableField(value = "modifi_date")
    private Date modifiDate;

    /**
     * 状态标识
     */
    @TableField(value = "bsblag")
    private String bsblag;

    public String getSx1() {
        return sx1;
    }

    public void setSx1(String sx1) {
        this.sx1 = sx1;
    }

    public String getSx2() {
        return sx2;
    }

    public void setSx2(String sx2) {
        this.sx2 = sx2;
    }

    public String getSx3() {
        return sx3;
    }

    public void setSx3(String sx3) {
        this.sx3 = sx3;
    }

    public String getSx4() {
        return sx4;
    }

    public void setSx4(String sx4) {
        this.sx4 = sx4;
    }

    public String getSx5() {
        return sx5;
    }

    public void setSx5(String sx5) {
        this.sx5 = sx5;
    }

    public String getSx6() {
        return sx6;
    }

    public void setSx6(String sx6) {
        this.sx6 = sx6;
    }

    public String getSx7() {
        return sx7;
    }

    public void setSx7(String sx7) {
        this.sx7 = sx7;
    }

    public String getSx8() {
        return sx8;
    }

    public void setSx8(String sx8) {
        this.sx8 = sx8;
    }

    public String getSx9() {
        return sx9;
    }

    public void setSx9(String sx9) {
        this.sx9 = sx9;
    }

    public String getSx10() {
        return sx10;
    }

    public void setSx10(String sx10) {
        this.sx10 = sx10;
    }

    /**
     * 设置明细值
     *
     * @param i  索引
     * @param mx 明细值
     */
    public void setMx(int i, String mx) {
        if (mx==null){
            return;
        }
        switch (i) {
            case 0:
                this.sx1 = mx;
                break;
            case 1:
                this.sx2 = mx;
                break;
            case 2:
                this.sx3 = mx;
                break;
            case 3:
                this.sx4 = mx;
                break;
            case 4:
                this.sx5 = mx;
                break;
            case 5:
                this.sx6 = mx;
                break;
            case 6:
                this.sx7 = mx;
                break;
            case 7:
                this.sx8 = mx;
                break;
            case 8:
                this.sx9 = mx;
                break;
            case 9:
                this.sx10 = mx;
                break;
        }
    }
}
