package com.drool.engine.constant;

import com.drool.engine.entity.HZjbgMx;
import com.drool.engine.mapper.oracle.Fp0001Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * @author 菜心
 * @date 2020/8/20
 * 优税管家 指标取值
 */
@Component
public class YsgjZbConstants {
    @Autowired

    private Fp0001Mapper fp0001Mapper;
    /**
     * 预收账款变动预警
     */
    public final static String ZB0201CW00059 = "F_资产负债表_年初预收账款,F_资产负债表_期末预收账款,F_资产负债表_年初应收账款,F_资产负债表_期末应收账款,F_资产负债表_年初应收票据,F_资产负债表_期末应收票据,F_利润表_营业收入_本年";
    /**
     * 预付账款变动预警
     */
    public final static String ZB0201CW00060 = "F_资产负债表_年初预付账款,F_资产负债表_期末预付账款,F_资产负债表_年初应付账款,F_资产负债表_期末应付账款,F_资产负债表_年初应付票据,F_资产负债表_期末应付票据,F_利润表_营业成本_本年,F_资产负债表_年初存货,F_资产负债表_期末存货";
    /**
     * 工程物资变动弹性系数预警
     */
    public final static String ZB0201CW00061 = "F_资产负债表_年初工程工资,F_资产负债表_期末工程工资,F_资产负债表_年初在建工程,F_资产负债表_期末在建工程,F_资产负债表_年初固定资产原值,F_资产负债表_期末固定资产原值";
    /**
     * 生产性生物资产变动率预警
     */
    public final static String ZB0201CW00062 = "F_资产负债表_年初生产性生物资产,F_资产负债表_期末生产性生物资产,F_利润表_营业成本_本年,F_利润表_营业收入_本年,F_利润表_财务费用_本年,F_利润表_管理费用_本年,F_利润表_销售费用_本年,F_利润表_营业成本_本年-基期,F_利润表_营业收入_本年-基期,F_利润表_财务费用_本年-基期,F_利润表_管理费用_本年-基期,F_利润表_销售费用_本年-基期";
    /**
     * 油气资产变动率预警
     */
    public final static String ZB0201CW00063 = "F_资产负债表_年初油气资产,F_资产负债表_期末油气资产,F_利润表_营业成本_本年,F_利润表_营业收入_本年,F_利润表_财务费用_本年,F_利润表_管理费用_本年,F_利润表_销售费用_本年,F_利润表_营业成本_本年-基期,F_利润表_营业收入_本年-基期,F_利润表_财务费用_本年-基期,F_利润表_管理费用_本年-基期,F_利润表_销售费用_本年-基期";

    /**
     * 开发支出变动率预警
     */
    public final static String ZB0201CW00064 = "F_资产负债表_年初开发支出,F_资产负债表_期末开发支出,F_利润表_营业成本_本年,F_利润表_营业收入_本年,F_利润表_财务费用_本年,F_利润表_管理费用_本年,F_利润表_销售费用_本年,F_资产负债表_年初无形资产,F_资产负债表_期末无形资产,F_利润表_营业成本_本年-基期,F_利润表_营业收入_本年-基期,F_利润表_财务费用_本年-基期,F_利润表_管理费用_本年-基期,F_利润表_销售费用_本年-基期";

    /**
     * 商誉变动率预警
     */
    public final static String ZB0201CW00065 = "F_资产负债表_年初商誉,F_资产负债表_期末商誉,F_资产负债表_年初长期股权投资,F_资产负债表_期末长期股权投资";
    /**
     * 递延所得税资产变动率预警
     */

    public final static String ZB0201CW00066 = "F_资产负债表_年初递延所得税财产,F_资产负债表_期末递延所得税财产";




    /**
     * 递延所得税资产变动率预警
     */

    public  List<HZjbgMx> JQs(String   qjq, String  qjz, String zbbh, String djxh){
        if (zbbh == null) {
            return null;
        }
        switch (zbbh) {
            case "01FP0001":
                return fp0001Mapper.getFp0001Sjx(qjq,qjz,djxh);
                default:
                    return   null;
        }

    }
    /**
     * 根据指标编号获取指标取值函数名
     *
     * @param zbbh 指标编号
     * @return
     */
    public static String[ ] getZbFunctionName(String zbbh) {
        if (zbbh == null) {
            return null;
        }
        switch (zbbh) {
            case "0201CW00059":
                return ZB0201CW00059.split(",");
            case "0201CW00060":
                return ZB0201CW00060.split(",");
            case "0201CW00061":
                return ZB0201CW00061.split(",");
            case "0201CW00062":
                return ZB0201CW00062.split(",");
            case "0201CW00063":
                return ZB0201CW00063.split(",");
            case "0201CW00064":
                return ZB0201CW00064.split(",");
            case "0201CW00065":
                return ZB0201CW00065.split(",");
            case "0201CW00066":
                return ZB0201CW00066.split(",");
            default:
                return null;
        }
    }
}
