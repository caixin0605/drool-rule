package com.drool.engine.config;
import com.baomidou.mybatisplus.core.MybatisConfiguration;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import javax.sql.DataSource;

/**
 * @author Leitian
 * @Description htckself数据库配置
 * @date 2018/12/27 0027
 */
@Configuration
@MapperScan(basePackages = {"com.drool.engine.mapper.oracle.**"},
        sqlSessionTemplateRef = "oracleSessionTemplate"
)
@EnableTransactionManagement
public class JsDataSource {
    @Bean(name = "orcleDataSource")
    @ConfigurationProperties(prefix = "spring.datasource.oracledata")
    @Primary
    public DataSource oracleDataSource() {
        return DataSourceBuilder.create().type(com.alibaba.druid.pool.DruidDataSource.class).build();
    }
    @Bean(name = "oracleSqlSessionFactory")
    @Primary
    public SqlSessionFactory orcleSqlSessionFactory(@Qualifier("orcleDataSource") DataSource dataSource) throws Exception {
        SqlSessionFactoryBean bean = new SqlSessionFactoryBean();
        bean.setDataSource(dataSource);
        //org.apache.ibatis.session.Configuration configuration = new org.apache.ibatis.session.Configuration();
        // 直接使用ibatis下的configuration会报错Invalid bound statement ，即无法绑定mapper，必须使用mybatis-plus下实现的MybatisConfiguration
        MybatisConfiguration configuration = new MybatisConfiguration();
        //注意acquistion下的FileData,FileDataLog需要开启驼峰映射
        configuration.setMapUnderscoreToCamelCase(true);
        bean.setConfiguration(configuration);
       // bean.setMapperLocations(new PathMatchingResourcePatternResolver().getResources("classpath:dataOracleMapper/*.xml"));
        return bean.getObject();
    }
    @Bean(name = "oracleTransactionManager")
    @Primary
    public PlatformTransactionManager oracleTransactionManager(@Qualifier("orcleDataSource") DataSource dataSource) {
        return new DataSourceTransactionManager(dataSource);
    }
    @Bean(name = "oracleSessionTemplate")
    @Primary
    public SqlSessionTemplate sqlSessionTemplate(@Qualifier("oracleSqlSessionFactory") SqlSessionFactory sqlSessionFactory) throws Exception {
        return new SqlSessionTemplate(sqlSessionFactory);
    }



    }
