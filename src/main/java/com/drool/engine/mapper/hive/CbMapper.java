package com.drool.engine.mapper.hive;

import com.drool.engine.config.HiveJdbcBaseDaoImpl;
import lombok.extern.log4j.Log4j;
import org.springframework.stereotype.Repository;

/**
 * @author 菜心
 * @date 2020/9/18
 * 财务报表 Mapper
 */
@Repository
@Log4j
public class CbMapper extends HiveJdbcBaseDaoImpl {
    /**
     * 获取资产负债表公共财务报表数据项
     *
     * @param djxh  登记序号
     * @param ssqq  所属期起
     * @param ssqz  所属期止
     * @param isQm  是否期末, true代表期末,false代表年初
     * @param about 数据在那边, 左就是true,右就是false (例:货币资金=true,短期借款=false)
     * @param qyh   企业准则行号
     * @param ybqyh 一般企业行号
     * @param xqyh  小企业行号
     * @return
     */
    public Double zcfzCommonDataItem(String djxh, String ssqq, String ssqz, Boolean isQm, Boolean about, String qyh, String ybqyh, String xqyh) {
        //1,获取最新上传的申报编号
        String zlbscjbidSql = "SELECT ZLBSCJBUUID FROM  cb_zlbscjb C WHERE C.DJ_ZZDAH= ?  AND C.YXQ_KS= ?  AND C.YXQ_JZ= ? LIMIT 1";
        Object[] zlbscjbidParams = new Object[]{djxh, ssqq, ssqz};
        String zlbscjbid = this.getJdbcTemplate().queryForObject(zlbscjbidSql, zlbscjbidParams, String.class);
        //取数列 : 定义是期末还是年初
        String qyColumn = "";
        String ybqyColumn = "";
        String xqyColumn = "";
        //左右 and 除末取数
        if (about) {
            if (isQm) {
                qyColumn = "zc.qms_zc";
                ybqyColumn = "zc.qmye_zc";
                xqyColumn = "zc.qmye_zc";
            } else {
                qyColumn = "zc.ncs_zc";
                ybqyColumn = "zc.ncye_zc";
                xqyColumn = "zc.ncye_zc";
            }

        } else {
            if (isQm) {
                qyColumn = "zc.qms_qy";
                ybqyColumn = "zc.qmye_qy";
                xqyColumn = "zc.qmye_qy";
            } else {
                qyColumn = "zc.ncs_qy";
                ybqyColumn = "zc.ncye_qy";
                xqyColumn = "zc.ncye_qy";
            }
        }
        log.info("取数列 : " + qyColumn + " " + ybqyColumn + " " + xqyColumn);
        //企业会计准则取数
        Object[] qyParams = new Object[]{qyColumn, zlbscjbid, djxh, qyh, ssqq, ssqz};
        String qySQL = "  SELECT ? " +
                "  FROM cb_qykjzz_zcfzb zc, " +
                "       cb_zlbscjb cb " +
                "  WHERE cb.ZLBSCJBUUID = zc.ZLBSCJBUUID " +
                "  AND zc.ZLBSCJBUUID = ? " +
                "  AND cb.DJ_ZZDAH = ? " +
                "  AND zc.EWBHXH = ? " +
                "  AND cb.YXQ_KS= ? " +
                "  AND cb.YXQ_JZ= ? ";
        Double qyData = this.getJdbcTemplate().queryForObject(qySQL, qyParams, Double.class);
        log.info("企业会计准则 = " + qyData);

        //企业会计准则取数
        Object[] ybqyParams = new Object[]{ybqyColumn, zlbscjbid, djxh, qyh, ssqq, ssqz};
        String ybqySQL = "  SELECT ? " +
                "  FROM cb_qykjzzybqy_zcfzb zc, " +
                "       cb_zlbscjb cb " +
                "  WHERE cb.ZLBSCJBUUID = zc.ZLBSCJBUUID " +
                "  AND zc.ZLBSCJBUUID = ? " +
                "  AND cb.DJ_ZZDAH = ? " +
                "  AND zc.EWBHXH = ? " +
                "  AND cb.YXQ_KS= ? " +
                "  AND cb.YXQ_JZ= ? ";
        Double ybqyData = this.getJdbcTemplate().queryForObject(ybqySQL, ybqyParams, Double.class);
        log.info("一般企业会计准则 = " + ybqyData);

        //小企业会计准则
        Object[] xqyParams = new Object[]{xqyColumn, zlbscjbid, djxh, qyh, ssqq, ssqz};
        String xqySQL = "  SELECT ? " +
                "  FROM cb_xqykjzz_zcfzb zc, " +
                "       cb_zlbscjb cb " +
                "  WHERE cb.ZLBSCJBUUID = zc.ZLBSCJBUUID " +
                "  AND zc.ZLBSCJBUUID = ? " +
                "  AND cb.DJ_ZZDAH = ? " +
                "  AND zc.EWBHXH = ? " +
                "  AND cb.YXQ_KS= ? " +
                "  AND cb.YXQ_JZ= ? ";
        Double xqyData = this.getJdbcTemplate().queryForObject(xqySQL, xqyParams, Double.class);
        log.info("小企业会计准则 = " + xqyData);
        if (qyData != null) {
            return qyData;
        } else if (ybqyData != null) {
            return ybqyData;
        } else if (xqyData != null) {
            return xqyData;
        } else {
            return null;
        }
    }

    /**
     * 获取利润表公共财务报表数据项
     *
     * @param djxh  登记序号
     * @param ssqq  所属期起
     * @param ssqz  所属期止
     * @param isBn  是否本年, true代表本年,false代表本期
     * @param qyh   企业准则行号
     * @param ybqyh 一般企业行号
     * @param xqyh  小企业行号
     * @return
     */
    public Double lrbCommonDataItem(String djxh, String ssqq, String ssqz, Boolean isBn, String qyh, String ybqyh, String xqyh) {
        //1,获取最新上传的申报编号
        String zlbscjbidSql = "SELECT ZLBSCJBUUID FROM  cb_zlbscjb C WHERE C.DJ_ZZDAH= ?  AND C.YXQ_KS= ?  AND C.YXQ_JZ= ? LIMIT 1";
        Object[] zlbscjbidParams = new Object[]{djxh, ssqq, ssqz};
        String zlbscjbid = this.getJdbcTemplate().queryForObject(zlbscjbidSql, zlbscjbidParams, String.class);
        //取数列 : 定义是期末还是年初
        String qyColumn = "";
        String ybqyColumn = "";
        String xqyColumn = "";
        //是本年还是本期
        if (isBn) {
            qyColumn = "lr.bnljs";
            ybqyColumn = "lr.bqje";
            xqyColumn = "lr.bnlj_je";
        } else {
            qyColumn = "lr.bys";
            ybqyColumn = "lr.sqje";
            xqyColumn = "lr.by_je";
        }
        //企业会计准则取数
        Object[] qyParams = new Object[]{qyColumn, zlbscjbid, djxh, qyh, ssqq, ssqz};
        String qySQL = " SELECT ? FROM cb_qykjzz_lrb lr, cb_zlbscjb cb " +
                "        WHERE cb.ZLBSCJBUUID = lr.ZLBSCJBUUID  " +
                "        AND lr.ZLBSCJBUUID= ? " +
                "        AND cb.DJ_ZZDAH = ? " +
                "        AND lr.EWBHXH = ? " +
                "        AND DATE_FORMAT( cb.YXQ_KS, '%Y-%m-%d' ) = DATE_FORMAT( ? , '%Y-%m-%d' )  " +
                "        AND DATE_FORMAT( cb.YXQ_JZ, '%Y-%m-%d' ) = DATE_FORMAT( ? , '%Y-%m-%d' )  ";
        return 0.0;
    }

}
