package com.drool.engine.mapper.hive;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * @author 菜心
 * @date 2020/9/18
 * 配置函数获取数据项
 */
@Repository
public class HiveFunctionFetch {
    @Autowired
    private CbMapper zcfzbMapper;
    private SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");

    /**
     * 获取数据项
     *
     * @param functionName 取数函数名称
     * @return
     */
    public Double fetchData(String functionName, String djxh, String ssqq, String ssqz) throws ParseException {

        if (functionName == null) {
            return null;
        }
        if (functionName.startsWith("F_资产负债表")) {
            switch (functionName) {
                case "F_资产负债表_年初持有待售负债余款":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, false, false, null, "12", null);
                case "F_资产负债表_年初持有待售资产余款":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, false, true, null, "11", null);
                case "F_资产负债表_年初持有至到期投资":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, false, true, null, "17", null);
                case "F_资产负债表_年初存货":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, false, true, "10", "10", "9");
                case "F_资产负债表_年初递延收益余额":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, false, false, null, "24", "15");
                case "F_资产负债表_年初短期借款余额":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, false, false, "1", "1", "1");
                case "F_资产负债表_年初短期投资":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, false, true, null, "2", "2");
                case "F_资产负债表_年初非流动资产合计":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, false, true, null, "33", "30");
                case "F_资产负债表_年初负债总额":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, false, false, "26", "28", "18");
                case "F_资产负债表_年初工程工资":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, false, true, "24", "23", "23");
                case "F_资产负债表_年初固定资产净值":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, false, true, "22", "21", "21");
                case "F_资产负债表_年初固定资产清理":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, false, true, "26", "24", "24");
                case "F_资产负债表_年初固定资产原值":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, false, true, "19", "21", "19");
                case "F_资产负债表_年初货币资金":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, false, true, "1", "1", "1");
                case "F_资产负债表_年初开发支出":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, false, true, null, "28", "27");
                case "F_资产负债表_年初可供出售金融资产":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, false, true, null, "16", null);
                case "F_资产负债表_年初流动资产合计":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, false, true, "14", "14", "15");
                case "F_资产负债表_年初其他非流动负债":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, false, false, null, "26", "16");
                case "F_资产负债表_年初其他非流动资产":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, false, true, null, "32", "29");
                case "F_资产负债表_年初其他流动负债":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, false, false, "14", "14", "10");
                case "F_资产负债表_年初其他流动资产":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, false, true, "13", "13", "14");
                case "F_资产负债表_年初其他应付账款":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, false, false, "10", "11", "9");
                case "F_资产负债表_年初其他应收款":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, false, true, "7", "9", "8");
                case "F_资产负债表_年初其它综合收益":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, false, false, null, "36", null);
                case "F_资产负债表_年初商誉":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, false, true, null, "29", null);
                case "F_资产负债表_年初生产性生物资产":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, false, true, null, "25", "25");
                case "F_资产负债表_年初实收资本余额":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, false, false, "28", "30", "26");
                case "F_资产负债表_年初所有者权益":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, false, false, "35", "39", "30");
                case "F_资产负债表_年初投资性房地产":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, false, true, null, "20", null);
                case "F_资产负债表_年初未分配利润":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, false, false, "34", "38", "29");
                case "F_资产负债表_年初无形资产":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, false, true, "29", "27", "26");
                case "F_资产负债表_年初一年内到期的非流动资产":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, false, true, null, "12", null);
                case "F_资产负债表_年初以公允价值计量且其变动计入当期损益的金融资":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, false, true, null, "2", null);
                case "F_资产负债表_年初应付票据":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, false, false, "2", "4", "2");
                case "F_资产负债表_年初应付账款":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, false, false, "3", "5", "3");
                case "F_资产负债表_年初应收利息":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, false, true, "5", "7", "7");
                case "F_资产负债表_年初应收票据":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, false, true, "3", "4", "3");
                case "F_资产负债表_年初应收账款":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, false, true, "6", "5", "4");
                case "F_资产负债表_年初油气资产":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, false, true, null, "26", null);
                case "F_资产负债表_年初预付账款":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, false, true, "8", "6", "5");
                case "F_资产负债表_年初预计负债余款":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, false, false, "12", "23", null);
                case "F_资产负债表_年初预收账款":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, false, false, "4", "6", "4");
                case "F_资产负债表_年初在建工程":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, false, true, "25", "22", "22");
                case "F_资产负债表_年初长期待摊费用":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, false, true, "30", "30", "28");
                case "F_资产负债表_年初长期股权投资":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, false, true, "16", "19", "18");
                case "F_资产负债表_年初长期借款余款":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, false, false, "17", "17", "13");
                case "F_资产负债表_年初长期应付款":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, false, false, "19", "21", "14");
                case "F_资产负债表_年初长期债券投资":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, false, true, null, "17", "17");
                case "F_资产负债表_年初专项应付款余款":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, false, false, null, "20", "22");
                case "F_资产负债表_年初资本公积余额":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, false, false, "31", "34", "27");
                case "F_资产负债表_年初资产总额":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, false, true, "36", "40", "31");
                case "F_资产负债表_年初递延所得税财产":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, false, true, null, "31", null);
                case "F_资产负债表_期末持有待售负债余款":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, true, false, null, "12", null);
                case "F_资产负债表_期末持有待售资产余款":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, true, true, null, "11", null);
                case "F_资产负债表_期末持有至到期投资":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, true, true, null, "17", null);
                case "F_资产负债表_期末存货":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, true, true, "10", "10", "9");
                case "F_资产负债表_期末存货_上期":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, dateToLastQuarter(ssqq), dateLastDay(dateToLastQuarter(ssqz)), true, true, "10", "10", "9");
                case "F_资产负债表_期末递延收益余额":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, true, false, null, "24", "15");
                case "F_资产负债表_期末短期借款余额":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, true, false, "1", "1", "1");
                case "F_资产负债表_期末短期投资":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, true, true, null, "2", "2");
                case "F_资产负债表_期末非流动资产合计":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, true, true, null, "33", "30");
                case "F_资产负债表_期末负债总额":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, true, false, "26", "28", "18");
                case "F_资产负债表_期末工程工资":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, true, true, "24", "23", "23");
                case "F_资产负债表_期末固定资产合计":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, true, true, "27", "21", "19");
                case "F_资产负债表_期末固定资产净值":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, true, true, "22", "21", "21");
                case "F_资产负债表_期末固定资产清理":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, true, true, "26", "24", "24");
                case "F_资产负债表_期末固定资产原值":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, true, true, "19", "21", "19");
                case "F_资产负债表_期末固定资产原值_上期":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, dateToLastQuarter(ssqq), dateLastDay(dateToLastQuarter(ssqz)), true, true, "19", "21", "19");
                case "F_资产负债表_期末货币资金":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, true, true, "1", "1", "1");
                case "F_资产负债表_期末开发支出":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, true, true, null, "28", "27");
                case "F_资产负债表_期末可供出售金融资产":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, true, true, null, "16", null);
                case "F_资产负债表_期末流动负债合计":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, true, false, "15", "15", "11");
                case "F_资产负债表_期末流动资产合计":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, true, true, "14", "14", "15");
                case "F_资产负债表_期末其他非流动负债":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, true, false, null, "26", "16");
                case "F_资产负债表_期末其他非流动资产":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, true, true, null, "32", "29");
                case "F_资产负债表_期末其他非流动资产_上期":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, dateToLastQuarter(ssqq), dateLastDay(dateToLastQuarter(ssqz)), true, true, null, "32", "29");
                case "F_资产负债表_期末其他流动负债":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, true, false, "14", "14", "10");
                case "F_资产负债表_期末其他流动资产":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, true, true, "13", "13", "14");
                case "F_资产负债表_期末其他流动资产_上期":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, dateToLastQuarter(ssqq), dateLastDay(dateToLastQuarter(ssqz)), true, true, "13", "13", "14");
                case "F_资产负债表_期末其他应付账款":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, true, false, "10", "11", "9");
                case "F_资产负债表_期末其他应付账款_上期":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, dateToLastQuarter(ssqq), dateLastDay(dateToLastQuarter(ssqz)), true, false, "10", "11", "9");
                case "F_资产负债表_期末其他应收款":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, true, true, "7", "9", "8");
                case "F_资产负债表_期末其他应收款_基期":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, dateToBaseQuarter(ssqq), dateLastDay(dateToBaseQuarter(ssqz)), true, true, "7", "9", "8");
                case "F_资产负债表_期末其他应收款_上期":
                    return zcfzbMapper.zcfzCommonDataItem(djxh,dateToLastQuarter(ssqq), dateLastDay(dateToLastQuarter(ssqz)), true, true, "7", "9", "8");
                case "F_资产负债表_期末其它综合收益":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, true, false, null, "36", null);
                case "F_资产负债表_期末商誉":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, true, true, null, "29", null);
                case "F_资产负债表_期末生产性生物资产":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, true, true, null, "25", "25");
                case "F_资产负债表_期末实收资本余额":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, true, false, "28", "30", "26");
                case "F_资产负债表_期末所有者权益":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, true, false, "35", "39", "30");
                case "F_资产负债表_期末投资性房地产":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, true, true, null, "20", null);
                case "F_资产负债表_期末未分配利润":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, true, false, "34", "38", "29");
                case "F_资产负债表_期末无形资产":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, true, true, "29", "27", "26");
                case "F_资产负债表_期末一年内到期的非流动资产":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, true, true, null, "12", null);
                case "F_资产负债表_期末以公允价值计量且其变动计入当期损益的金融资":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, true, true, null, "2", null);
                case "F_资产负债表_期末应付票据":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, true, false, "2", "4", "2");
                case "F_资产负债表_期末应付债券":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, true, false, "18", "18", null);
                case "F_资产负债表_期末应付账款":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, true, false, "3", "5", "3");
                case "F_资产负债表_期末应付账款_上期":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, dateToLastQuarter(ssqq), dateLastDay(dateToLastQuarter(ssqz)), true, false, "3", "5", "3");
                case "F_资产负债表_期末应收利息":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, true, true, "5", "7", "7");
                case "F_资产负债表_期末应收票据":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, true, true, "3", "4", "3");
                case "F_资产负债表_期末应收账款":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, true, true, "6", "5", "4");
                case "F_资产负债表_期末应收账款_上期":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, dateToLastQuarter(ssqq), dateLastDay(dateToLastQuarter(ssqz)), true, true, "6", "5", "4");
                case "F_资产负债表_期末油气资产":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, true, true, null, "26", null);
                case "F_资产负债表_期末预付账款":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, true, true, "8", "6", "5");
                case "F_资产负债表_期末预计负债余款":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, true, false, "12", "23", null);
                case "F_资产负债表_期末预收账款":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, true, false, "4", "6", "4");
                case "F_资产负债表_期末在建工程":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, true, true, "25", "22", "22");
                case "F_资产负债表_期末长期待摊费用":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, true, true, "30", "30", "28");
                case "F_资产负债表_期末长期股权投资":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, true, true, "16", "19", "18");
                case "F_资产负债表_期末长期借款余款":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, true, false, "17", "17", "13");
                case "F_资产负债表_期末长期应付款":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, true, false, "19", "21", "14");
                case "F_资产负债表_期末长期债券投资":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, true, true, null, "17", "17");
                case "F_资产负债表_期末专项应付款余款":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, true, false, null, "20", "22");
                case "F_资产负债表_期末资本公积余额":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, true, false, "31", "34", "27");
                case "F_资产负债表_期末资产总额":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, true, true, "36", "40", "31");
                case "F_资产负债表_期末递延所得税财产":
                    return zcfzbMapper.zcfzCommonDataItem(djxh, ssqq, ssqz, true, true, null, "31", null);
            }
        }
/*        if (hsm.startsWith("F_利润表")) {
            switch (hsm) {
                case "F_利润表_财务费用_本年":
                    this.cwfyBn_lrb = data;
                    break;
                case "F_利润表_财务费用_本年_基期":
                    this.cwfyBn_jq_lrb = data;
                    break;
                case "F_利润表_公允价值变动收益损失_本年":
                    this.gyjzbdsyssBn_lrb = data;
                    break;
                case "F_利润表_管理费用_本年":
                    this.glfyBn_lrb = data;
                    break;
                case "F_利润表_管理费用_本年_基期":
                    this.glfyBn_jq_lrb = data;
                    break;
                case "F_利润表_净利润_本年":
                    this.jlrBn_lrb = data;
                    break;
                case "F_利润表_净利润_本年_基期":
                    this.jlrBn_jq_lrb = data;
                    break;
                case "F_利润表_利润总额_本年":
                    this.lrzeBn_lrb = data;
                    break;
                case "F_利润表_利润总额_本年_基期":
                    this.lrzeBn_jq_lrb = data;
                    break;
                case "F_利润表_其他收益_本年":
                    this.qtsyBn_lrb = data;
                    break;
                case "F_利润表_税金及附加_本年":
                    this.sjjfjBn_lrb = data;
                    break;
                case "F_利润表_投资收益_本年":
                    this.tzsyBn_lrb = data;
                    break;
                case "F_利润表_销售费用_本年":
                    this.xsfyBn_lrb = data;
                    break;
                case "F_利润表_销售费用_本年_基期":
                    this.xsfyBn_jq_lrb = data;
                    break;
                case "F_利润表_营业成本_本年":
                    this.yycbBn_lrb = data;
                    break;
                case "F_利润表_营业成本_本年_上期":
                    this.yycbBn_sq_lrb = data;
                    break;
                case "F_利润表_营业成本_本年_基期":
                    this.yycbBn_jq_lrb = data;
                    break;
                case "F_利润表_营业利润_本年":
                    this.yylrBn_lrb = data;
                    break;
                case "F_利润表_营业利润_本年_基期":
                    this.yylrBn_jq_lrb = data;
                    break;
                case "F_利润表_营业收入_本年":
                    this.yysrBn_lrb = data;
                    break;
                case "F_利润表_营业收入_本年_上期":
                    this.yysrBn_sq_lrb = data;
                    break;
                case "F_利润表_营业收入_本年_基期":
                    this.yysrBn_jq_lrb = data;
                    break;
                case "F_利润表_营业外收入_本年":
                    this.yywsrBn_lrb = data;
                    break;
                case "F_利润表_营业外支出_本年":
                    this.yywzcBn_lrb = data;
                    break;
                case "F_利润表_资产处置收益_本年":
                    this.zcczsyBn_lrb = data;
                    break;
                case "F_利润表_资产减值损失_本年":
                    this.zcjzssBn_lrb = data;
                    break;
            }
        }*/
/*        if (hsm.startsWith("F_现金流量表")) {
            switch (hsm) {
                case "F_现金流量表_处置固定资产、无形资产和其他长期资产、非流动资产收回的现金净额_本年":
                    this.czgdzcwxzchqtcqzcfldzcshdxjjeBn_xjllb = data;
                    break;
                case "F_现金流量表_经营活动产生的现金流净额_本年":
                    this.jyhdcsdxjljeBn_xjllb = data;
                    break;
                case "F_现金流量表_经营活动现金流量净额_本年":
                    this.jyhdxjlljeBn_xjllb = data;
                    break;
                case "F_现金流量表_收到的其他与经营活动有关的资金_本年":
                    this.sddqtyjyhdygdzjBn_xjllb = data;
                    break;
                case "F_现金流量表_销售商品提供劳务收到的现金_本年":
                    this.xssptglwsddxjBn_xjllb = data;
                    break;
            }
        }*/
/*        if (hsm.startsWith("F_所得税")) {
            switch (hsm) {
                case "F_所得税_减免所得税额_年累":
                    this.jmsdseNl_sds = data;
                    break;
                case "F_所得税_利润总额_本期":
                    this.lrzeBq_sds = data;
                    break;
                case "F_所得税_利润总额_年累":
                    this.lrzeNl_sds = data;
                    break;
                case "F_所得税_营业成本_本期":
                    this.yycbBq_sds = data;
                    break;
                case "F_所得税_营业成本_年累":
                    this.yycbNl_sds = data;
                    break;
                case "F_所得税_营业收入_本期":
                    this.yysrBq_sds = data;
                    break;
                case "F_所得税_营业收入_年累":
                    this.yysrNl_sds = data;
                    break;
                case "F_所得税_应纳所得税额_本期":
                    this.ynsdseBq_sds = data;
                    break;
                case "F_所得税_应纳所得税额_年累":
                    this.ynsdseNl_sds = data;
                    break;
            }
        }*/
/*        if (hsm.startsWith("F_一般纳税人")) {
            switch (hsm) {
                case "F_一般纳税人_减税项目_减免明细表":
                    this.jsxmjmmxb_ybnsr = data;
                    break;
                case "F_一般纳税人_税控系统专用设备费及技术维护费_附表4":
                    this.xkxtzysbfjjswhffb4_ybnsr = data;
                    break;
                case "F_一般纳税人_本期进项税额转出额_附表2":
                    this.bqjxsezcefb2_ybnsr = data;
                    break;
                case "F_一般纳税人_非正常损失_附表2":
                    this.fzcssfb2_ybnsr = data;
                    break;
                case "F_一般纳税人_6税率销售额_附表1":
                    this.sl6xse_b1_ybnsr = data;
                    break;
                case "F_一般纳税人_6税率开具其他发票_附表1":
                    this.sl6kjqtfp_b1_ybnsr = data;
                    break;
                case "F_一般纳税人_6税率开具增值税专用发票_附表1":
                    this.sl6kjzzszyfp_b1_ybnsr = data;
                    break;
                case "F_一般纳税人_16税率销售额_附表1":
                    this.sl16xse_b1_ybnsr = data;
                    break;
                case "F_一般纳税人_期末留抵税额_本年":
                    this.qmldseBn_ybnsr = data;
                    break;
                case "F_一般纳税人_销项税额_本年":
                    this.xxseBn_ybnsr = data;
                    break;
                case "F_一般纳税人_进项税额_本年":
                    this.jxseBn_ybnsr = data;
                    break;
                case "F_一般纳税人_销项税额_本年_上期":
                    this.xxseBn_sq_ybnsr = data;
                    break;
                case "F_一般纳税人_进项税额_本年_上期":
                    this.jxseBn_sq_ybnsr = data;
                    break;
                case "F_一般纳税人_简易办法纳税检查调整_本年":
                    this.jybfnsjctzBn_ybnsr = data;
                    break;
                case "F_一般纳税人_免抵退销售收入_本年":
                    this.mdtxxsr_ybnsr = data;
                    break;
                case "F_一般纳税人_应纳税额合计_本年":
                    this.ynsehjBn_ybnsr = data;
                    break;
                case "F_一般纳税人_应纳税额合计_本期":
                    this.ynsehjBq_ybnsr = data;
                    break;
                case "F_一般纳税人_应纳税额减征额_本年":
                    this.ynsejzeBn_ybnsr = data;
                    break;
                case "F_一般纳税人_免税销售额_本期":
                    this.msxseBq_ybnsr = data;
                    break;
                case "F_一般纳税人_免税销售额_本年":
                    this.msxseBn_ybnsr = data;
                    break;
                case "F_一般纳税人_不开票_销项应纳税额":
                    this.bkpxsynse_ybnsr = data;
                    break;
                case "F_一般纳税人_不开票收入":
                    this.bkpsr_ybnsr = data;
                    break;
                case "F_一般纳税人_当期申报抵扣进项税额合计_附表2":
                    this.dqsbdkjxsehjfb2_ybnsr = data;
                    break;
                case "F_一般纳税人_既征增值税销售额_本年":
                    this.jzzzsxseBn_ybnsr = data;
                    break;
                case "F_一般纳税人_开具其他发票_销项应纳税额":
                    this.kjqtfpxsynse_ybnsr = data;
                    break;
                case "F_一般纳税人_开具其他发票收入":
                    this.kjqtfpsr_ybnsr = data;
                    break;
                case "F_一般纳税人_农产品收购发票或者销售发票税额_附表2":
                    this.ncpsgfphzxsfpsefb2_ybnsr = data;
                    break;
                case "F_一般纳税人_申报抵扣进项税额_本年":
                    this.sbdkjxseBn_ybnsr = data;
                    break;
                case "F_一般纳税人_申报抵扣进项税额转出_本年":
                    this.sbdkjxsezcBn_ybnsr = data;
                    break;
                case "F_一般纳税人_销售额合计_附表1":
                    this.xsehjfb1_ybnsr = data;
                    break;
                case "F_一般纳税人_应纳销售额_本年":
                    this.ynxseBn_ybnsr = data;
                    break;
                case "F_一般纳税人_应纳销售额_本年_基期":
                    this.ynxseBn_jq_ybnsr = data;
                    break;
                case "F_一般纳税人_应纳销售额_本期":
                    this.ynxseBq_ybnsr = data;
                    break;
                case "F_一般纳税人_应纳销售额_本期_上期":
                    this.ynxseBq_sq_ybnsr = data;
                    break;
                case "F_一般纳税人_应纳增值税额_本年":
                    this.ynzzseBn_ybnsr = data;
                    break;
                case "F_一般纳税人_应纳增值税额_本年_上期":
                    this.ynzzseBn_sq_ybnsr = data;
                    break;
                case "F_一般纳税人_应纳增值税额_本年_基期":
                    this.ynzzseBn_jq_ybnsr = data;
                    break;
                case "F_一般纳税人_专用发票_销项应纳税额":
                    this.zyfpxxynse_ybnsr = data;
                    break;
                case "F_一般纳税人_专用发票收入":
                    this.zyfpsr_ybnsr = data;
                    break;
            }
        }*/
/*        if (hsm.startsWith("F_小规模纳税人")) {
            switch (hsm) {
                case "F_小规模纳税人_税务机关代开的增值税专用发票不含税销售额_本年":
                    this.swjgdkdzzszyfpbhsxse_xgm = data;
                    break;
                case "F_小规模纳税人_税控器具开具的普通发票不含税销售额_本年":
                    this.skqjkjdptfpbhsxse_xgm = data;
                    break;
                case "F_小规模纳税人_应征增值税不含税销售额3_本年":
                    this.yzzzsbhxse3Bn_xgm = data;
                    break;
                case "F_小规模纳税人_减税项目_减免明细表":
                    this.jsxmjmmxb_xgm = data;
                    break;
                case "F_小规模纳税人_销售使用过的固定资产不含税销售额_减免明细表":
                    this.xssygdgdzcbhsxsejmmxb_xgm = data;
                    break;
                case "F_小规模纳税人_免抵退销售收入_本年":
                    this.mdtxssr_xgm = data;
                    break;
                case "F_小规模纳税人_应纳税额合计_本年":
                    this.ynsehjBn_xgm = data;
                    break;
                case "F_小规模纳税人_应纳税额合计_本期":
                    this.ynsehjBq_xgm = data;
                    break;
                case "F_小规模纳税人_应纳税额减征额_本年":
                    this.ynsejzeBn_xgm = data;
                    break;
                case "F_小规模纳税人_应纳销售额_本年":
                    this.ynxseBn_xgm = data;
                    break;
                case "F_小规模纳税人_应纳销售额_本年_基期":
                    this.ynxseBn_jq_xgm = data;
                    break;
                case "F_小规模纳税人_应纳销售额_本期":
                    this.ynxseBq_xgm = data;
                    break;
                case "F_小规模纳税人_应纳销售额_本期_上期":
                    this.ynxseBq_sq_xgm = data;
                    break;
                case "F_小规模纳税人_应纳增值税_本年":
                    this.ynzzsBn_xgm = data;
                    break;
                case "F_小规模纳税人_免税销售额_本期":
                    this.msxseBq_xgm = data;
                    break;
            }
        }*/
        return null;
    }

    /**
     * 日期转上季度
     *
     * @param date
     * @return
     */
    public String dateToLastQuarter(String date) throws ParseException {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(sf.parse(date));
        System.out.println(sf.parse(date));
        calendar.add(Calendar.MONTH, -3);
        return sf.format(calendar.getTime());
    }

    /**
     * 日期转基期
     *
     * @param date
     * @return
     */
    public String dateToBaseQuarter(String date) throws ParseException {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(sf.parse(date));
        System.out.println(sf.parse(date));
        calendar.add(Calendar.YEAR, -1);
        return sf.format(calendar.getTime());
    }

    /**
     * 获取当前日期最后一天
     *
     * @param date
     * @return
     * @throws ParseException
     */
    public String dateLastDay(String date) throws ParseException {
        Calendar calendar = Calendar.getInstance();
        System.out.println(sf.parse(date));
        calendar.setTime(sf.parse(date));
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        return sf.format(calendar.getTime());
    }
}
