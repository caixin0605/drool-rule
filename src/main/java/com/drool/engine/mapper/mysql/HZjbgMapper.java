package com.drool.engine.mapper.mysql;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.drool.engine.entity.HRelationEnterprise;
import com.drool.engine.entity.HZjbg;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 指标总汇表mapper
 *
 * @author xincai
 * @date 2020-08-18
 */
public interface HZjbgMapper extends BaseMapper<HZjbg> {
    /**
     * 查询企业基本信息
     * @param djxh
     * @return
     */
    @Select("SELECT h.nsrmc,h.nsrsbh FROM h_relation_enterprise h  WHERE h.djxh=#{djxh} AND h.bsblag='0' ")
    HRelationEnterprise queryHRelationEnterprise(String djxh);

    /**
     * 删除报告(模型)运行结果
     * @param bgbh
     */
    @Select("DELETE    FROM  h_zjbg          WHERE  bgbh=#{bgbh}  AND bsblag='0'")
    void deleteByBgbh(String bgbh);
}
