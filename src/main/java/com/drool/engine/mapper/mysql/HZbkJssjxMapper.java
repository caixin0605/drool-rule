package com.drool.engine.mapper.mysql;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.drool.engine.entity.HZbJssjx;
import org.apache.ibatis.annotations.Select;

import java.util.List;


/**
 * @author 菜心
 * @date 2020/8/18
 * 指标库mapper
 */
public interface HZbkJssjxMapper extends BaseMapper<HZbJssjx> {
    /**
     * 根据模型id查询指标
     *
     * @param mxId 模型id
     * @return
     */
    @Select("SELECT * FROM h_zb_jssjx where   zbbh=#{zbbh}")
    List<HZbJssjx> queryZbJssjx(String  zbbh);

}
