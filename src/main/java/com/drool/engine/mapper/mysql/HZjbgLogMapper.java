package com.drool.engine.mapper.mysql;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.drool.engine.entity.HZjbgLog;

/**
 * 指标日志记录表mapper
 *
 * @author xincai
 * @date 2020-08-18
 */
public interface HZjbgLogMapper extends BaseMapper<HZjbgLog> {

}
