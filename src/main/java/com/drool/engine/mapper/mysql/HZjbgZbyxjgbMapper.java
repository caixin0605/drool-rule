package com.drool.engine.mapper.mysql;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.drool.engine.entity.HZjbgZbyxjgb;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * 指标结果表mapper
 *
 * @author xincai
 * @date 2020-08-18
 */
public interface HZjbgZbyxjgbMapper extends BaseMapper<HZjbgZbyxjgb> {
    /**
     * 运行的指标数量
     *
     * @param djxh 登记序号
     * @param bgbh 报告编号
     * @param mxid 模型id
     * @return
     */
    @Select(" SELECT COUNT(1) FROM h_zjbg_zbyxjgb  WHERE bgbh=#{bgbh}  AND djxh=#{djxh} AND  sjlx_dm='1' AND zbbh IN" +
            " (SELECT yszbbh FROM  xt_fxmx_sjgx WHERE fx_bj='01'   AND mxuuid=#{mxid}  AND bsblag='0' )")
    Long queryRunZbCount(@Param("djxh") String djxh, @Param("bgbh") String bgbh, @Param("mxid") String mxid);

    /**
     * 风险指标数量
     *
     * @param djxh 登记序号
     * @param bgbh 报告编号
     * @param mxid 模型id
     * @return
     */
    @Select(" SELECT COUNT(1) FROM h_zjbg_zbyxjgb WHERE bgbh=#{bgbh} AND djxh=#{djxh} AND  zbjg='Y' AND sjlx_dm='1' " +
            " AND zbbh IN(SELECT yszbbh FROM  xt_fxmx_sjgx WHERE fx_bj='01' AND mxuuid=#{mxid}  AND bsblag='0' )")
    Long queryFxZbCount(@Param("djxh") String djxh, @Param("bgbh") String bgbh, @Param("mxid") String mxid);

    /**
     * 缺数据项指标数量
     *
     * @param djxh 登记序号
     * @param bgbh 报告编号
     * @param mxid 模型id
     * @return
     */
    @Select(" SELECT COUNT(1) FROM h_zjbg_zbyxjgb WHERE bgbh=#{bgbh} AND djxh=#{djxh} AND sjlx_dm='1' AND sjwz_bz='2' " +
            " AND zbbh IN(SELECT yszbbh FROM  xt_fxmx_sjgx WHERE fx_bj='01' AND mxuuid=#{mxid}  AND bsblag='0' )")
    Long queryQueDataItemCount(@Param("djxh") String djxh, @Param("bgbh") String bgbh, @Param("mxid") String mxid);

    /**
     * 删除指标结果表数据
     *
     * @param bgbh 报告编号
     */
    @Select(" DELETE  FROM  h_zjbg_zbyxjgb  WHERE  bgbh=#{bgbh}  AND bsblag='0'")
    void deleteByBgbh(String bgbh);
}
