package com.drool.engine.mapper.mysql;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.drool.engine.entity.XtFxmxSjgx;

/**
 * @author 菜心
 * @date 2020/8/19
 * 模型数据关系表mapper
 */
public interface XtFxmxSjgxMapper extends BaseMapper<XtFxmxSjgx> {

}
