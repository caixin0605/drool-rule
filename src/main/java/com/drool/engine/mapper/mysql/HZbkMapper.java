package com.drool.engine.mapper.mysql;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.drool.engine.entity.HRelationEnterprise;
import com.drool.engine.entity.HZbk;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.mapping.StatementType;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @author 菜心
 * @date 2020/8/18
 * 指标库mapper
 */
public interface HZbkMapper extends BaseMapper<HZbk> {
    /**
     * 根据模型id查询指标
     *
     * @param mxId 模型id
     * @return
     */
    @Select("SELECT syfw,zbbh,gzwj,gzwj_lx,zbmc,gwfx FROM h_zbk WHERE  qybs='0' AND  zbbh IN  (SELECT yszbbh  FROM  xt_fxmx_sjgx WHERE  mxuuid=#{mxId}  AND  bsblag='0')  AND  zbbh IN (select zbbh  from  h_zb_jssjx)")
    List<HZbk> queryZbkByMxId(String mxId);


    @Select("{ #{Data,mode=OUT,jdbcType=DECIMAL}=call ${functionName}( " +
            " #{I_ZZDAH,mode=IN,jdbcType=VARCHAR}, " + //登记序号
            " #{I_QJQ,mode=IN,jdbcType=DATE}, " + //数据区间起
            " #{I_QJZ,mode=IN,jdbcType=DATE}) }")
    @Options(statementType = StatementType.CALLABLE)
    void queryDataItem(Map<String, Object> params);

    /**
     * 查询数据项基期
     *
     * @param params
     */
    @Select("{ #{Data,mode=OUT,jdbcType=DECIMAL}=call ${functionName}( " +
            " #{I_ZZDAH,mode=IN,jdbcType=VARCHAR}, " + //登记序号
            " DATE_SUB(#{I_QJQ,mode=IN,jdbcType=DATE}, INTERVAL 1 YEAR) , " + //数据区间起
            " DATE_SUB(#{I_QJZ,mode=IN,jdbcType=DATE}, INTERVAL 1 YEAR)) }")
    @Options(statementType = StatementType.CALLABLE)
    void queryDataItemToBasePeriod(Map<String, Object> params);

    /**
     * 查询数据项上期
     *
     * @param params
     */
    @Select("{ #{Data,mode=OUT,jdbcType=DECIMAL}=call ${functionName}( " +
            " #{I_ZZDAH,mode=IN,jdbcType=VARCHAR}, " + //登记序号
            " DATE_SUB(#{I_QJQ,mode=IN,jdbcType=DATE}, INTERVAL 3 MONTH) , " + //数据区间起
            " LAST_DAY(DATE_SUB(#{I_QJZ,mode=IN,jdbcType=DATE}, INTERVAL 3 MONTH)) ) }")
    @Options(statementType = StatementType.CALLABLE)
    void queryDataItemToThePeriod(Map<String, Object> params);

    //动态执行适用范围的sql语句
    @Select("${sql}")
    Long execusql(@Param("sql") String sql);
}
