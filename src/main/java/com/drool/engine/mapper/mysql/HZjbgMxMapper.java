package com.drool.engine.mapper.mysql;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.drool.engine.entity.HZjbgMx;
import org.apache.ibatis.annotations.Select;

/**
 * 指标运行结果明细Mapper
 *
 * @author xincai
 * @date 2020-08-18
 */
public interface HZjbgMxMapper extends BaseMapper<HZjbgMx> {
    /**
     * 删除明细数据
     * @param bgbh 报告编号
     */
    @Select("DELETE    FROM  h_zjbg_mx       WHERE  bgbh=#{bgbh}  AND bsblag='0'")
    void deleteByBgbh(String bgbh);
}
