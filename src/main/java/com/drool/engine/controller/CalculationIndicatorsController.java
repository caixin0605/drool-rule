package com.drool.engine.controller;

import com.drool.engine.entity.HZjbgMx;
import com.drool.engine.entity.Result;
import com.drool.engine.mapper.hive.CbMapper;
import com.drool.engine.mapper.mysql.HZbkMapper;
import com.drool.engine.mapper.oracle.Fp0001Mapper;
import com.drool.engine.service.CaiShuiWeiShiService;
import com.drool.engine.service.CalculationIndicatorsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * @author 指标公共计算入口、采用规则引擎
 * fly
 * 梳理逻辑
 * @date 2020/8/18
 */

@RestController
@RequestMapping("ysgj/csws")
public class CalculationIndicatorsController {

    @Autowired
    private CalculationIndicatorsService calculationIndicatorsService;
    @Autowired
    private CaiShuiWeiShiService caiShuiWeiShiService;
    @Autowired
    private HZbkMapper hZbkMapper;
    @Autowired
    private Fp0001Mapper fp0001Mapper;

    @PostMapping("/ziJianBaoGao")
    public Result ziJianBaoGao(@RequestParam String bgbh,
                               @RequestParam String djxh,
                               @RequestParam String qymc,
                               @RequestParam String nsrsbh,
                               @RequestParam String zjqjq,
                               @RequestParam String zjqjz,
                               @RequestParam String mxbh) throws ParseException {
        return caiShuiWeiShiService.cswsZjbgForSC(bgbh, djxh, qymc, nsrsbh, zjqjq, zjqjz, mxbh);
    }

    //oracle 数据源测试方法
    @PostMapping("/testoracle")
    public List<HZjbgMx> testoracle() throws ParseException {

        List<HZjbgMx> fp0001Sjx = calculationIndicatorsService.getFp0001Sjx("2019-01-01", "2019-03-31", "1212121");

        return fp0001Sjx;
    }

    @Autowired
    private CbMapper testMapper;

    @PostMapping("/hive")
    public Double testHive() throws ParseException {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
/*        HiveParamBean paramBean = new HiveParamBean();
        paramBean.setDjxh("65664482503364312242");
        paramBean.setSsqq(df.parse("2019-04-01"));
        paramBean.setSsqz(df.parse("2019-06-30"));
        paramBean.setIsQm(true);
        paramBean.setQyh("1");
        paramBean.setYbqyh("1");
        paramBean.setXqyh("1");*/
        //return testMapper.commonDataItem(paramBean);
        return null;//testMapper.ZcfzbCommonDataItem("65664482503364312242","2019-04-01","2019-06-30",true,true,"1","1","1");
    }

}
